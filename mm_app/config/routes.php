<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller']    = 'inicio';
$route['404_override']          = '';
$route['translate_uri_dashes']  = FALSE;

$route['admin']                 = 'admin/inicio';
$route['admin/mi-perfil']       = 'admin/mi_perfil';
$route['admin/salir']           = 'admin/login/salir';

$route['admin/clientes/(:num)/contactos'] = 'admin/clientes/mostrar_contactos/$1';