<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = [];

$autoload['libraries'] = ['ion_auth','form_validation'];

$autoload['drivers'] = [];

$autoload['helper'] = ['url','language'];

$autoload['config'] = [];

$autoload['language'] = [];

$autoload['model'] = [];