<?php

class Admin_api extends API_CONTROLLER{ //ADMIN_Controller {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
    }

    //-------------------------------------------------------------------------------------------------------------------------
    // CLIENTES
    //-------------------------------------------------------------------------------------------------------------------------
    function insert_cliente(){
        //Validacion de Form
        $this->form_validation->set_rules('name', 'Nombre', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->output->set_output(json_encode(array(
                'result' => 0,
                'error' => $this->form_validation->error_array()
            )));
            return false;
        }

        //Validación del correo único
        $this->load->model('clientes_model');
        $result = $this->clientes_model->insert(array(
            'name'          => $this->input->post('name'),
            'street'        => $this->input->post('street'),
            'ext_num'       => $this->input->post('ext_num'),
            'int_num'       => $this->input->post('int_num'),
            'colony'        => $this->input->post('colony'),
            'cp'            => $this->input->post('cp'),
            'municipality'  => $this->input->post('municipality'),
            'state'         => $this->input->post('state'),
            'created_on'    => time(),
            'active'        => '1'
        ));
        if ($result) {
            $this->output->set_output(json_encode(array(
                'result' => 1
            )));
            return false;
        }
        return false;
    }

    function update_cliente(){
        //Validacion de Form
        $this->form_validation->set_rules('name', 'Nombre', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->output->set_output(json_encode(array(
                'result' => 0,
                'error' => $this->form_validation->error_array()
            )));
            return false;
        }

        //Validación del correo único
        $this->load->model('clientes_model');

        $data = [
            'name'          => $this->input->post('name'),
            'street'        => $this->input->post('street'),
            'ext_num'       => $this->input->post('ext_num'),
            'int_num'       => $this->input->post('int_num'),
            'colony'        => $this->input->post('colony'),
            'cp'            => $this->input->post('cp'),
            'municipality'  => $this->input->post('municipality'),
            'state'         => $this->input->post('state'),
        ];

        $this->clientes_model->update($data, $this->input->post('id'));
        $this->output->set_output(json_encode(array(
            'result' => 1
        )));
    }


    //-------------------------------------------------------------------------------------------------------------------------
    // TURNOS
    //-------------------------------------------------------------------------------------------------------------------------
    function insert_turno(){
        //Validacion de Form
        $this->form_validation->set_rules('name', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('description', 'Descripción', 'trim');
        $this->form_validation->set_rules('time_start', 'Hora de Inicio', 'trim|required');
        $this->form_validation->set_rules('time_end', 'Hora de Fin', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->output->set_output(json_encode(array(
                'result' => 0,
                'error' => $this->form_validation->error_array()
            )));
            return false;
        }

        //Validación del correo único
        $this->load->model('turnos_model');
        $result = $this->turnos_model->insert(array(
            'name'              => $this->input->post('name'),
            'description'       => $this->input->post('description'),
            'time_start'        => $this->input->post('time_start'),
            'time_end'          => $this->input->post('time_end'),
            'created_on'        => time(),
            'active'            => '1'
        ));
        if ($result) {
            $this->output->set_output(json_encode(array(
                'result' => 1
            )));
            return false;
        }
        return false;
    }

    function update_turno(){
        //Validacion de Form
        $this->form_validation->set_rules('name', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('description', 'Descripción', 'trim');
        $this->form_validation->set_rules('time_start', 'Hora de Inicio', 'trim|required');
        $this->form_validation->set_rules('time_end', 'Hora de Fin', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->output->set_output(json_encode(array(
                'result' => 0,
                'error' => $this->form_validation->error_array()
            )));
            return false;
        }

        //Validación del correo único
        $this->load->model('turnos_model');

        $data = [
            'name'              => $this->input->post('name'),
            'description'       => $this->input->post('description'),
            'time_start'        => $this->input->post('time_start'),
            'time_end'          => $this->input->post('time_end'),
        ];

        $this->turnos_model->update($data, $this->input->post('id'));
        $this->output->set_output(json_encode(array(
            'result' => 1
        )));
    }

    //-------------------------------------------------------------------------------------------------------------------------
    // AREAS
    //-------------------------------------------------------------------------------------------------------------------------
    function insert_area(){
        //Validacion de Form
        $this->form_validation->set_rules('name', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('description', 'Descripción', 'trim');

        if ($this->form_validation->run() == false) {
            $this->output->set_output(json_encode(array(
                'result' => 0,
                'error' => $this->form_validation->error_array()
            )));
            return false;
        }

        //Validación del correo único
        $this->load->model('areas_model');
        $result = $this->areas_model->insert(array(
            'name'              => $this->input->post('name'),
            'description'       => $this->input->post('description'),
            'created_on'        => time(),
            'active'            => '1'
        ));
        if ($result) {
            $this->output->set_output(json_encode(array(
                'result' => 1
            )));
            return false;
        }
        return false;
    }

    function update_area(){
        //Validacion de Form
        $this->form_validation->set_rules('name', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('description', 'Descripción', 'trim');

        if ($this->form_validation->run() == false) {
            $this->output->set_output(json_encode(array(
                'result' => 0,
                'error' => $this->form_validation->error_array()
            )));
            return false;
        }

        //Validación del correo único
        $this->load->model('areas_model');

        $data = [
            'name'              => $this->input->post('name'),
            'description'       => $this->input->post('description'),
        ];

        $this->areas_model->update($data, $this->input->post('id'));
        $this->output->set_output(json_encode(array(
            'result' => 1
        )));
    }


    //-------------------------------------------------------------------------------------------------------------------------
    // PROVEEDORES
    //-------------------------------------------------------------------------------------------------------------------------
    function insert_proveedor(){
        //Validacion de Form
        $this->form_validation->set_rules('name', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('service_number', 'No. de Servicio', 'trim');

        if ($this->form_validation->run() == false) {
            $this->output->set_output(json_encode(array(
                'result' => 0,
                'error' => $this->form_validation->error_array()
            )));
            return false;
        }

        //Validación del correo único
        $this->load->model('proveedores_model');
        $result = $this->proveedores_model->insert(array(
            'name'              => $this->input->post('name'),
            'service_number'    => $this->input->post('service_number'),
            'created_on'        => time(),
            'active'            => '1'
        ));
        if ($result) {
            $this->output->set_output(json_encode(array(
                'result' => 1
            )));
            return false;
        }
        return false;
    }

    function update_proveedor(){
        //Validacion de Form
        $this->form_validation->set_rules('name', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('description', 'Descripción', 'trim');

        if ($this->form_validation->run() == false) {
            $this->output->set_output(json_encode(array(
                'result' => 0,
                'error' => $this->form_validation->error_array()
            )));
            return false;
        }

        $this->load->model('proveedores_model');

        $data = [
            'name'              => $this->input->post('name'),
            'service_number'    => $this->input->post('service_number'),
        ];

        $this->proveedores_model->update($data, $this->input->post('id'));
        $this->output->set_output(json_encode(array(
            'result' => 1
        )));
    }

    //-------------------------------------------------------------------------------------------------------------------------
    // HTC
    //-------------------------------------------------------------------------------------------------------------------------
    function insert_htc(){
        //Validacion de Form
        $this->form_validation->set_rules('id_client', 'Cliente', 'trim|required');
        $this->form_validation->set_rules('id_plant', 'Planta', 'trim|required');
        $this->form_validation->set_rules('code', 'Código', 'trim|required');
        $this->form_validation->set_rules('part_number', 'No. Parte', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->output->set_output(json_encode(array(
                'result' => 0,
                'error' => $this->form_validation->error_array()
            )));
            return false;
        }

        $this->load->model('htcs_model');
        $result = $this->htcs_model->insert(array(
            'id_provider'     => $this->input->post('id_client'),
            'id_client'      => $this->input->post('id_plant'),
            'code'          => $this->input->post('code'),
            'part_number'   => $this->input->post('part_number'),
            'created_on'    => time(),
            'active'        => '1'
        ));
        if ($result) {
            $this->output->set_output(json_encode(array(
                'result' => 1
            )));
            return false;
        }
        return false;
    }

    function update_htc(){
        //Validacion de Form
        $this->form_validation->set_rules('id_client', 'Cliente', 'trim|required');
        $this->form_validation->set_rules('id_plant', 'Planta', 'trim|required');
        $this->form_validation->set_rules('code', 'Código', 'trim|required');
        $this->form_validation->set_rules('part_number', 'No. Parte', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->output->set_output(json_encode(array(
                'result' => 0,
                'error' => $this->form_validation->error_array()
            )));
            return false;
        }

        $this->load->model('htcs_model');

        $data = [
            'id_provider'   => $this->input->post('id_client'),
            'id_client'     => $this->input->post('id_plant'),
            'code'          => $this->input->post('code'),
            'part_number'   => $this->input->post('part_number'),
        ];

        $result = $this->htcs_model->update($data, $this->input->post('id'));
        if($result > 0){
            $this->output->set_output(json_encode(array(
                'result' => 1
            )));
        }else{
            $this->output->set_output(json_encode(array(
                'result' => 0
            )));

        }
    }

    //-------------------------------------------------------------------------------------------------------------------------
    // FALLAS
    //-------------------------------------------------------------------------------------------------------------------------
    function insert_falla(){
        $this->form_validation->set_rules('description', 'Descripción', 'trim');

        if ($this->form_validation->run() == false) {
            $this->output->set_output(json_encode(array(
                'result' => 0,
                'error' => $this->form_validation->error_array()
            )));
            return false;
        }

        $this->load->model('failures_model');

        $id_lot = $this->input->post('id_lot');
        $imagen = $this->input->post('image');

        if (file_exists(FCPATH . TMP_DIR . $imagen) && $imagen != '') {
            if (!is_dir(FCPATH . FAILURES_FOLDER . md5($id_lot))) {
                mkdir(FCPATH . FAILURES_FOLDER . md5($id_lot));
            }
            rename(FCPATH . TMP_DIR . $imagen, FCPATH . FAILURES_FOLDER . md5($id_lot) .'/'. $imagen);
        }

        $result = $this->failures_model->insert(array(
            'image'             => $imagen,
            'description'       => $this->input->post('description'),
            'id_htc'            => $id_lot,
            'created_on'        => time(),
            'active'            => 1
        ));
        if ($result) {
            $this->output->set_output(json_encode(array(
                'result' => 1
            )));
            return false;
        }
        $this->output->set_output(json_encode(array(
            'result' => 0,
            'error' => 'No se pudo insertar la falla'
        )));
    }

    function update_falla(){
        $this->form_validation->set_rules('description', 'Descripción', 'trim');

        if ($this->form_validation->run() == false) {
            $this->output->set_output(json_encode(array(
                'result' => 0,
                'error' => $this->form_validation->error_array()
            )));
            return false;
        }
        $this->load->model('failures_model');

        $item = $this->failures_model->get(['id_failure' => $this->input->post('id')]);

        $id_lot = $this->input->post('id_lot');
        $imagen = $this->input->post('image');

        if($item[0]->image != $imagen){
            if (file_exists(FCPATH . TMP_DIR . $imagen) && $imagen != '') {
                if (!is_dir(FCPATH . FAILURES_FOLDER. md5($id_lot))) {
                    mkdir(FCPATH . FAILURES_FOLDER. md5($id_lot));
                }
                rename(FCPATH . TMP_DIR . $imagen, FCPATH . FAILURES_FOLDER . md5($id_lot) .'/'. $imagen);
            }
        }

        $result = $this->failures_model->update(array(
            'image'         => $imagen,
            'description'   => $this->input->post('description'),
        ), $this->input->post('id'));

        $this->output->set_output(json_encode(array(
            'result' => 1
        )));
        return false;
    }


    //-------------------------------------------------------------------------------------------------------------------------
    // OPERADORES
    //-------------------------------------------------------------------------------------------------------------------------
    function insert_operador(){
        //VAlidacion de Form
        $this->form_validation->set_rules('username', 'No. Operador', 'required');
        $this->form_validation->set_rules('first_name', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('last_name', 'Paterno', 'trim|required');
        $this->form_validation->set_rules('last_name_2', 'Materno', 'trim|required');
        $this->form_validation->set_rules('id_plant', 'Planta', 'required');
        $this->form_validation->set_rules('id_area', 'Área', 'required');
        $this->form_validation->set_rules('id_turn', 'Turno', 'required');
        $this->form_validation->set_rules('phone', 'Teléfono', 'trim');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', 'Confirmar Password', 'required');

        $email    = strtolower($this->input->post('email'));
        $identity = $this->input->post('username');
        $password = $this->input->post('password');

        $additional_data = array(
            'id_plant'      => $this->input->post('id_plant'),
            'id_area'       => $this->input->post('id_area'),
            'id_turn'       => $this->input->post('id_turn'),
            'first_name'    => $this->input->post('first_name'),
            'last_name'     => $this->input->post('last_name'),
            'last_name_2'   => $this->input->post('last_name_2'),
            'phone'         => $this->input->post('phone'),
        );

        $result = $this->ion_auth->register($identity, $password, $email, $additional_data);
        if ($result) {
            $this->output->set_output(json_encode(array(
                'result' => 1
            )));
            return false;
        }
        return false;
    }

    function update_operador(){
        $this->form_validation->set_rules('username', 'No. Operador', 'required');
        $this->form_validation->set_rules('first_name', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('last_name', 'Paterno', 'trim|required');
        $this->form_validation->set_rules('last_name_2', 'Materno', 'trim|required');
        $this->form_validation->set_rules('id_plant', 'Planta', 'required');
        $this->form_validation->set_rules('id_area', 'Área', 'required');
        $this->form_validation->set_rules('id_turn', 'Turno', 'required');
        $this->form_validation->set_rules('phone', 'Teléfono', 'trim');
        if ($this->input->post('password'))
        {
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
            $this->form_validation->set_rules('password_confirm', 'Confirmar Password', 'required');
        }

        $data = array(
            'id_plant'      => $this->input->post('id_plant'),
            'id_area'       => $this->input->post('id_area'),
            'id_turn'       => $this->input->post('id_turn'),
            'first_name'    => $this->input->post('first_name'),
            'last_name'     => $this->input->post('last_name'),
            'last_name_2'   => $this->input->post('last_name_2'),
            'email'         => $this->input->post('email'),
            'phone'         => $this->input->post('phone'),
        );

        // update the password if it was posted
        if ($this->input->post('password'))
        {
        	$data['password'] = $this->input->post('password');
        }

        $this->ion_auth->update($this->input->post('id'), $data);
        $this->output->set_output(json_encode(array(
            'result' => 1
        )));
    }


    function update_usuario() {

        $this->form_validation->set_rules('first_name', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('last_name', 'Paterno', 'trim|required');
        $this->form_validation->set_rules('last_name_2', 'Materno', 'trim|required');
        $this->form_validation->set_rules('email', 'Correo Electrónico', 'trim|required|valid_email');
        $this->form_validation->set_rules('phone', 'Teléfono', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->output->set_output(json_encode(array(
                'result' => 0,
                'error' => $this->form_validation->error_array()
            )));
            return false;
        }

        //Validación del correo único
        $this->load->model('users_model');
        $sql = array(
            'first_name'    => $this->input->post('first_name'),
            'last_name'     => $this->input->post('last_name'),
            'last_name_2'   => $this->input->post('last_name_2'),
            'email'         => $this->input->post('email'),
            'phone'         => $this->input->post('phone'),
        );

        $this->users_model->update($sql, $this->input->post('id'));
        $this->output->set_output(json_encode(array(
            'result' => 1
        )));
    }

    function insert_contacto(){
        $this->form_validation->set_rules('first_name', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('last_name', 'Paterno', 'trim|required');
        $this->form_validation->set_rules('last_name_2', 'Materno', 'trim|required');
        $this->form_validation->set_rules('email', 'Correo Electrónico', 'trim|required|valid_email');
        $this->form_validation->set_rules('phone', 'Teléfono', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', 'Confirmar Password', 'required');

        $email      = strtolower($this->input->post('email'));
        $identity   = $this->input->post('username');
        $password   = $this->input->post('password');
        $id_cliente = $this->input->post('id_cliente');

        $additional_data = array(
            'id_plant'      => NULL,
            'id_area'       => NULL,
            'id_turn'       => NULL,
            'first_name'    => $this->input->post('first_name'),
            'last_name'     => $this->input->post('last_name'),
            'last_name_2'   => $this->input->post('last_name_2'),
            'phone'         => $this->input->post('phone'),
        );

        $result = $this->ion_auth->register($identity, $password, $email, $additional_data, ['3']);

        if ($result) {

            $this->load->model('clientes_has_contactos_model');

            $this->clientes_has_contactos_model->insert([
                'id_client'     => $id_cliente,
                'id_contact'    => $result
            ]);

            $this->output->set_output(json_encode(array(
                'result' => 1
            )));
            return false;
        }
        $this->output->set_output(json_encode(array(
            'result' => 0
        )));
        return false;
    }

    function update_contacto(){
        $this->form_validation->set_rules('first_name', 'Nombre', 'trim|required');
        $this->form_validation->set_rules('last_name', 'Paterno', 'trim|required');
        $this->form_validation->set_rules('last_name_2', 'Materno', 'trim|required');
        $this->form_validation->set_rules('email', 'Correo Electrónico', 'trim|required|valid_email');
        $this->form_validation->set_rules('phone', 'Teléfono', 'trim|required');
        $password = $this->input->post('passsword');
        if($password != FALSE){
            $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
            $this->form_validation->set_rules('password_confirm', 'Confirmar Password', 'required');
        }

        if ($this->form_validation->run() == false) {
            $this->output->set_output(json_encode(array(
                'result' => 0,
                'error' => $this->form_validation->error_array()
            )));
            return false;
        }

        //Validación del correo único
        $this->load->model('users_model');
        $data = [
            'first_name'    => $this->input->post('first_name'),
            'last_name'     => $this->input->post('last_name'),
            'last_name_2'   => $this->input->post('last_name_2'),
            'email'         => $this->input->post('email'),
            'phone'         => $this->input->post('phone'),
        ];

        if($password != FALSE){
            $data['password'] = $password;
        }

        $this->users_model->update($data, $this->input->post('id'));
        $this->output->set_output(json_encode(array(
            'result' => 1
        )));
    }

    function update_box(){
        $this->form_validation->set_rules('box_number', 'No. de Caja', 'trim');
        $this->form_validation->set_rules('pieces_number', 'No. de Pieza', 'trim|required');
        $this->form_validation->set_rules('lote', 'Lote', 'trim|required');
        $this->form_validation->set_rules('serial', 'Serial', 'trim|required');
        $this->form_validation->set_rules('nok_pieces', 'Piezas NO OK', 'trim|required');
        $this->form_validation->set_rules('production_time', 'T. de Producción', 'trim|required');
        $this->form_validation->set_rules('pause_time', 'T. de Pausa', 'trim|required');

        if ($this->form_validation->run() == false) {
            $this->output->set_output(json_encode(array(
                'result' => 0,
                'error' => $this->form_validation->error_array()
            )));
            return false;
        }

        //Validación del correo único
        $this->load->model('boxes_model');
        $data = [
            'box_number'        => $this->input->post('box_number'),
            'pieces_number'     => $this->input->post('pieces_number'),
            'lote'              => $this->input->post('lote'),
            'serial'            => $this->input->post('serial'),
            'nok_pieces'        => $this->input->post('nok_pieces'),
            'production_time'   => $this->input->post('production_time'),
            'pause_time'        => $this->input->post('pause_time'),
        ];

        $this->boxes_model->update($data, $this->input->post('id'));
        $this->output->set_output(json_encode(array(
            'result' => 1
        )));
    }

    function upload_tmp_image(){
        $config['upload_path']          = './tmp/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 0;
        $config['max_width']            = 0;
        $config['max_height']           = 0;
        $config['file_ext_tolower']     = TRUE;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('image'))
        {
            $result = 0;
            $data = array('error' => $this->upload->display_errors());
        }
        else
        {
            $result = 1;
            $data = array('upload_data' => $this->upload->data());
        }

        $this->output->set_output(json_encode(array(
            'result' => $result,
            'data'  => $data
        )));

    }
}