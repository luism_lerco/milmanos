<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');
        date_default_timezone_set('America/Mexico_City');
    }

    public function index() {
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url('admin/login'), 'refresh');
        }
        elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
        {
            $css  = $this->load->view('admin/assets/css/datatable', '', true);
            $css .= $this->load->view('admin/assets/css/reportes', '', true);
            $this->hdata = [
                'css'        => $css,
                'title'     => 'Panel Reportes',
                'breadcumb' => [
                    [
                        'url'   => base_url('admin/inicio'),
                        'label' => 'Panel'
                    ],
                    [
                        'label' => 'Reportes'
                    ]
                ],
            ];

            $columns  = '"columns" : [';
            $columns .= '{ className : "v-align-middle text-center" },';
            $columns .= '{ className : "v-align-middle text-center" },';
            $columns .= '{ className : "v-align-middle text-center" },';
            $columns .= '{ className : "v-align-middle text-center" },';
            $columns .= '{ className : "v-align-middle text-center" },';
            $columns .= '{ className : "v-align-middle text-center" },';
            if($this->ion_auth->is_admin()){
                $columns .= '{ className : "v-align-middle text-center" },';
                $columns .= '{ className : "v-align-middle text-center" },';
                $columns .= '{ className : "v-align-middle text-center" },';
                $columns .= '{ className : "v-align-middle text-center" },';
            }
            $columns .= '],';

            $this->js    = [
                'items'       => base_url('admin/reportes/get_datatable'),
                'columns'     => $columns,
            ];

            $this->data = [
                'URL_REPORTE'  => base_url('admin/reportes/mostrar_generar'),
            ];

            $js  = $this->load->view('admin/assets/js/datatable', $this->js, true);
            $js .= $this->load->view('admin/assets/js/reportes', '', true);

            $this->fdata = [
                'js'            => $js,
                'link_active'    => ['#liReportes', '#lkProduccion']
            ];

            $this->load->view('admin/commons/header_view', $this->hdata);
            $this->load->view('admin/reportes/index_view', $this->data);
            $this->load->view('admin/commons/footer_view', $this->fdata);
        }
        else
        {
            $css  = $this->load->view('admin/assets/css/datatable', '', true);
            $css .= $this->load->view('admin/assets/css/reportes', '', true);
            $this->hdata = [
                'css'        => $css,
                'title'     => 'Panel Reportes',
                'breadcumb' => [
                    [
                        'url'   => base_url('admin/inicio'),
                        'label' => 'Panel'
                    ],
                    [
                        'label' => 'Reportes'
                    ]
                ],
            ];

            $columns  = '"columns" : [';
            $columns .= '{ className : "v-align-middle text-center" },';
            $columns .= '{ className : "v-align-middle text-center" },';
            $columns .= '{ className : "v-align-middle text-center" },';
            $columns .= '{ className : "v-align-middle text-center" },';
            $columns .= '{ className : "v-align-middle text-center" },';
            $columns .= '{ className : "v-align-middle text-center" },';
            $columns .= '{ className : "v-align-middle text-center" },';
            $columns .= '{ className : "v-align-middle text-center" },';
            $columns .= '{ className : "v-align-middle text-center" },';
            $columns .= '{ className : "v-align-middle text-center" },';
            $columns .= '],';

            $this->js    = [
                'items'       => base_url('admin/reportes/get_datatable'),
                'columns'     => $columns,
            ];

            $this->data = [
                'URL_REPORTE'  => base_url('admin/reportes/mostrar_generar'),
            ];

            $js  = $this->load->view('admin/assets/js/datatable', $this->js, true);
            $js .= $this->load->view('admin/assets/js/reportes', '', true);

            $this->fdata = [
                'js'            => $js,
                'link_active'    => ['#liReportes', '#lkProduccion']
            ];

            $this->load->view('admin/commons/header_view', $this->hdata);
            $this->load->view('admin/reportes/index_view', $this->data);
            $this->load->view('admin/commons/footer_view', $this->fdata);
        }
    }


    public function generar(){
        $this->form_validation->set_rules('id_client', 'Cliente', 'trim|required');
        $this->form_validation->set_rules('part_number', 'No.Parte', 'trim|required');
        $this->form_validation->set_rules('id_provider', 'Proveedor', 'trim|required');
        $this->form_validation->set_rules('fecha_inicio', 'Fecha Inicio', 'trim|required');
        $this->form_validation->set_rules('fecha_fin', 'Fecha Fin', 'trim|required');


        if ($this->form_validation->run() == false) {
            $this->output->set_output(json_encode(array(
                'result' => 0,
                'error' => $this->form_validation->error_array()
            )));
            return false;
        }

        $no_reporte                             = sha1(time());
        $reporte                                = [];
        $reporte[$no_reporte]                   = [];
        $reporte[$no_reporte]['id_client']      = $this->input->post('id_client');
        $reporte[$no_reporte]['part_number']    = $this->input->post('part_number');
        $reporte[$no_reporte]['id_provider']    = $this->input->post('id_provider');
        $reporte[$no_reporte]['fecha_inicio']   = $this->input->post('fecha_inicio');
        $reporte[$no_reporte]['fecha_fin']      = $this->input->post('fecha_fin');
        $reporte[$no_reporte]['defectos']       = $this->input->post('defectos');
        $this->session->set_userdata($reporte);

        $this->output->set_output(json_encode([
            'result'        => 1,
            'no_reporte'    => $no_reporte
        ]));
    }

    public function operadores(){
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url('admin/login'), 'refresh');
        }
        elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
        {
            $this->load->model(['clientes_model', 'clientes_has_contactos_model']);
            $this->hdata = [
                'title'     => 'Panel Reportes',
                'breadcumb' => [
                    [
                        'url'   => base_url('admin/inicio'),
                        'label' => 'Panel'
                    ],
                    [
                        'label' => 'Reportes'
                    ]
                ],
            ];

            $user = $this->ion_auth->user()->row();
            $chc = $this->clientes_has_contactos_model->get([
                'id_contact' => $user->id
            ]);

            $clients = [];

            foreach($chc as $i){
                $clients[] = $i->id_client;
            }

            $this->data = [
	            'clientes'	=> $this->get_clients($clients)
            ];

            $this->fdata = [
	            'js'			 => $this->load->view('admin/assets/js/operadores', '', true),
                'link_active'    => ['#liReportes', '#lkOperadores']
            ];

            $this->load->view('admin/commons/header_view', $this->hdata);
            $this->load->view('admin/reportes/operadores_view',$this->data);
            $this->load->view('admin/commons/footer_view', $this->fdata);
        }
        else
        {
	        $this->load->model('clientes_model');
            $this->hdata = [
                'title'     => 'Panel Reportes',
                'breadcumb' => [
                    [
                        'url'   => base_url('admin/inicio'),
                        'label' => 'Panel'
                    ],
                    [
                        'label' => 'Reportes'
                    ]
                ],
            ];

            $this->data = [
	            'clientes'	=> $this->get_clients()
            ];

            $this->fdata = [
	            'js'			 => $this->load->view('admin/assets/js/operadores', '', true),
                'link_active'    => ['#liReportes', '#lkOperadores']
            ];

            $this->load->view('admin/commons/header_view', $this->hdata);
            $this->load->view('admin/reportes/operadores_view',$this->data);
            $this->load->view('admin/commons/footer_view', $this->fdata);
         }
    }

    public function generado_csv($token){
        //validaciones
        if (!$this->ion_auth->logged_in())
        {
            return false;
        }
        elseif (!$this->ion_auth->in_group([1,3])) // remove this elseif if you want to enable this for non-admins
        {
            return false;
        }

        $info = $this->session->userdata($token);

        $this->load->model(['clientes_model', 'proveedores_model', 'htcs_model', 'failures_model', 'boxes_model', 'box_failures_model']);

        $cliente    = $this->clientes_model->get(['id_client' => $info['id_client']]);
        $proveedor  = $this->proveedores_model->get(['id_provider' => $info['id_provider']]);
        $htc        = $this->htcs_model->get(['htcs.id_client' => $info['id_client'], 'htcs.id_provider' => $info['id_provider'], 'part_number' => $info['part_number']]);

        $fecha_inicio   = $info['fecha_inicio'];
        $fecha_fin      = $info['fecha_fin'];

        if (strpos($info['fecha_inicio'], '/') !== false) {
            $fecha_inicio = str_replace('/','-',$info['fecha_inicio']);
        }

        if (strpos($info['fecha_fin'], '/') !== false) {
            $fecha_fin = str_replace('/','-',$info['fecha_fin']);
        }

        $cajas      = $this->boxes_model->get(['boxes.htc'=> $htc[0]->id_htc, 'DATE(boxes.created_on) >=' => date('Y-m-d', strtotime($fecha_inicio)), 'DATE(boxes.created_on) <=' => date('Y-m-d', strtotime($fecha_fin)) ]); //Cambiar a id_htc

        $filename    = TMP_DIR.microtime() . '.csv';
        $fp          = fopen($filename, 'w');
        $content     = ["#", "NO. DE PARTE", "LOTE", "SERIAL", "PZAS OK" , "PZAS NO OK" , "PZAS INSPECCIONADAS"];
        fputcsv($fp, $content);

        $detalle    = [];
        $t_ok = $t_nok = $t_pzas = 0;
        $boxes  = [];
        $fallas = [];
        if($cajas != false){
            $cont = 1;
            foreach($cajas as $c){
                $t_ok   += $c->pieces_number-$c->nok_pieces;
                $t_nok  += $c->nok_pieces;
                $t_pzas += $c->pieces_number;

                fputcsv($fp, [$cont, $c->part_number, $c->lote, $c->serial, $t_ok, $t_nok, $t_pzas]);
                $cont++;
            }
        }

        fclose($fp);


        $csv = [
            'filename'  => base_url($filename),
            'status'    => 'success',
        ];

        $this->output->set_output(json_encode($csv));
        return false;

    }

    public function generado($token) {
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url('admin/login'), 'refresh');
        }
        elseif (!$this->ion_auth->in_group([1,3])) // remove this elseif if you want to enable this for non-admins
        {
            redirect(base_url('admin/inicio'), 'refresh');
        }
        else
        {

            $info = $this->session->userdata($token);

            $this->load->model(['clientes_model', 'proveedores_model', 'htcs_model', 'failures_model', 'boxes_model', 'box_failures_model']);

            $cliente    = $this->clientes_model->get(['id_client' => $info['id_client']]);
            $proveedor  = $this->proveedores_model->get(['id_provider' => $info['id_provider']]);
            $htc        = $this->htcs_model->get(['htcs.id_client' => $info['id_client'], 'htcs.id_provider' => $info['id_provider'], 'part_number' => $info['part_number']]);

            $fecha_inicio   = $info['fecha_inicio'];
            $fecha_fin      = $info['fecha_fin'];

            if (strpos($info['fecha_inicio'], '/') !== false) {
                $fecha_inicio = str_replace('/','-',$info['fecha_inicio']);
            }

            if (strpos($info['fecha_fin'], '/') !== false) {
                $fecha_fin = str_replace('/','-',$info['fecha_fin']);
            }

            $cajas      = $this->boxes_model->get(['boxes.htc'=> $htc[0]->id_htc, 'DATE(boxes.created_on) >=' => date('Y-m-d', strtotime($fecha_inicio)), 'DATE(boxes.created_on) <=' => date('Y-m-d', strtotime($fecha_fin)) ]); //Cambiar a id_htc

            $detalle    = [];
            $t_ok = $t_nok = $t_pzas = 0;
            $boxes  = [];
            $fallas = [];
            if($cajas != false){
                foreach($cajas as $c){
                    $t_ok   += $c->pieces_number-$c->nok_pieces;
                    $t_nok  += $c->nok_pieces;
                    $t_pzas += $c->pieces_number;

                    $detalle[]  = [
                        'part_number' => $c->part_number,
                        'serial'      => $c->serial,
                        'pzas_caja'   => $c->pieces_number,
                        'no_caja'     => $c->box_number,
                        //'pzas_nok'    => $c->nok_pieces,
                        'pzas_nok'    => '<a href="'.base_url('admin/reportes/show_fallas/'.$c->id_box).'" class="show-modal" />'.$c->nok_pieces.'</a>',
                        'lote'        => $c->lote,//$htc[0]->code,
                    ];

                    $boxes[] = $c->id_box;
                }
                $box_failures = [];
                $box_failures['box'] = $boxes;
                if($info['defectos'] > 0){
                    $box_failures['box_failures.id_failure'] = $info['defectos'];
                }

                $fallas = $this->box_failures_model->get_items_in($box_failures);
            }

            $this->hdata = [
                'css'       => $this->load->view('admin/assets/css/generado', '', true),
                'title'     => 'Panel Reportes',
                'breadcumb' => [
                    [
                        'url'   => base_url('admin/inicio'),
                        'label' => 'Panel'
                    ],
                    [
                        'label' => 'Reportes'
                    ]
                ],
            ];

            $this->data = [
                'token'         => $token,
                'cliente'       => $cliente[0],
                'proveedor'     => $proveedor[0],
                'htc'           => $htc[0],
                'fallas'        => $detalle,
                'fecha_inicio'  => $fecha_inicio,
                'fecha_fin'     => $fecha_fin,
                'part_number'   => $info['part_number'],
                't_ok'          => $t_ok,
                't_nok'         => $t_nok,
                't_pzas'        => $t_pzas,
                'd_fallas'      => $fallas,
            ];

            $js = ['t_ok' => $t_ok, 't_nok' => $t_nok];

            $this->fdata = [
                'js'             => $this->load->view('admin/assets/js/generado', $js, true),
                'link_active'    => ['#liReportes']
            ];

            $this->load->view('admin/commons/header_view', $this->hdata);
            $this->load->view('admin/reportes/reporte_view', $this->data);
            $this->load->view('admin/commons/footer_view', $this->fdata);
        }
    }

    public function mostrar_generar(){
        $data = [];
        $this->load->model(['clientes_has_contactos_model']);
        $data['HEADER_MODAL']   = 'Generar Reporte';
        $data['URL_FORM']       = base_url('admin/reportes/generar');
        $user   = $this->ion_auth->user()->row();
        $chc    = $this->clientes_has_contactos_model->get([
            'id_contact' => $user->id
        ]);

        $clients = [];

        foreach($chc as $i){
            $clients[] = $i->id_client;
        }

        $data['clientes']       = $this->get_clients($clients);
        $this->_render_page('admin/reportes/generar_view', $data);
    }

    public function get_datatable(){
        $modelo = 'boxes_model';
        $this->load->model([$modelo, 'clientes_has_contactos_model']);

        // Datatables Variables
        $draw   = intval($this->input->get("draw"));
        $start  = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        if($this->ion_auth->is_admin()){
            $result = $this->$modelo->get(NULL , ['created_on DESC']);
        }else{
           $user = $this->ion_auth->user()->row();

            $chc = $this->clientes_has_contactos_model->get([
                'id_contact' => $user->id
            ]);

            $clients = [];

            foreach($chc as $i){
                $clients[] = $i->id_client;
            }

            $result = $this->$modelo->get(['clients.id_client' => implode(',',$clients)] , ['created_on DESC']);
        }

        $data   = [];

        foreach($result as $r) {
            if($this->ion_auth->is_admin()){
                $data[] = [
                    $r->provider,
                    $r->box_number,
                    $r->pieces_number,
                    $r->lote,
                    $r->serial,
                    '<a href="'.base_url('admin/reportes/show_fallas/'.$r->id_box).'" class="show-modal" />'.$r->nok_pieces.'</a>',
                    $r->production_time,
                    $r->pause_time,
                    $r->created_on,
                    '<a href="'.base_url('admin/reportes/edit_box/'.$r->id_box).'" class="show-modal btn btn-success btn-xs"><i class="fa fa-edit"></i> Editar</a>',
                ];
            }else{
                $data[] = [
                    $r->provider,
                    $r->box_number,
                    $r->pieces_number,
                    $r->lote,
                    $r->serial,
                    '<a href="'.base_url('admin/reportes/show_fallas/'.$r->id_box).'" class="show-modal" />'.$r->nok_pieces.'</a>',
                    $r->created_on,
                ];
            }
        }

        $output = [
            "draw"              => $draw,
            "recordsTotal"      => count($result),
            "recordsFiltered"   => count($result),
            "data"              => $data,
        ];

        $this->output->set_output(json_encode($output));
        return false;
    }

    public function _render_page($view, $data=null, $returnhtml=false)//I think this makes more sense
    {

        $this->viewdata = (empty($data)) ? $this->data: $data;
        $view_html = $this->load->view($view, $this->viewdata, $returnhtml);
        if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
    }

    public function get_clients($id = NULL){
        $items = [];
        $this->db->distinct();
        $this->db->select();
        if(!is_null($id)){
            if(is_array($id)){
                foreach($id as $i){
                    $this->db->where('id_client', $i);
                }
            }
            else{
                $this->db->where('id_client', $id);
            }
        }

        $q = $this->db->get('clients');
        if($q->result()){
            $items ['-1'] = '-- seleccione --';
            foreach($q->result() as $i){
                $items[$i->id_client] = $i->name;
            }
        }

        return $items;
    }

    public function get_serial_numbers(){
        $id = $this->input->post('id_client');
        $items = [];
        if (!is_null($id)) {
            $this->db->where('id_client', $id);
            $this->db->distinct();
            $this->db->select();
            $this->db->group_by('part_number');
            $q = $this->db->get('htcs');

            if($q->result()){
                $items ['-1'] = '-- seleccione --';
                foreach($q->result() as $i){
                    $items[$i->part_number] = $i->part_number;
                }
            }
        }

        $this->output->set_output(json_encode($items));
        return false;
    }

    public function get_operators(){
	    $id = $this->input->post('part_number');
	    $items = [];
	    if(!is_null($id)){
		    $this->db->where('htcs.part_number', $id);
		    $this->db->distinct();
		    $this->db->select('boxes.id_operator, CONCAT_WS(" ", last_name_2, last_name, first_name) AS operator');
		    $this->db->group_by('id_operator');
		    $this->db->from('boxes');
		    $this->db->join('htcs', 'htcs.id_htc=boxes.htc');
		    $this->db->join('users', 'users.id=boxes.id_operator');
		    $q = $this->db->get();

		    if($q->result()){
                $items ['-1'] = '-- seleccione --';
                foreach($q->result() as $i){
                    $items[$i->id_operator] = $i->operator;
                }
            }
	    }

	    $this->output->set_output(json_encode($items));
        return false;
    }

    public function get_proveedores(){
        $id_client      = $this->input->post('id_client');
        $part_number    = $this->input->post('part_number');
        $items = [];
        $this->db->where('id_client', $id_client);
        $this->db->where('part_number', $part_number);
        $this->db->distinct();
        $this->db->select();
        $this->db->from('htcs');
        $this->db->join('providers', 'providers.id_provider=htcs.id_provider');
        $this->db->group_by('htcs.id_provider');
        $q = $this->db->get();

        if($q->result()){
            $items ['-1'] = '-- seleccione --';
            foreach($q->result() as $i){
                $items[$i->id_provider] = $i->name;
            }
        }

        $this->output->set_output(json_encode($items));
        return false;
    }

    public function get_fallas(){
        $id_client      = $this->input->post('id_client');
        $part_number    = $this->input->post('part_number');
        $id_provider    = $this->input->post('id_provider');
        $items = [];
        $this->db->where('id_client', $id_client);
        $this->db->where('part_number', $part_number);
        $this->db->where('id_provider', $id_provider);
        $this->db->select('id_htc');
        $q = $this->db->get('htcs');
        if($q->result()){
            $id_htc = $q->row()->id_htc;
            $this->load->model('failures_model');
            $fallas = $this->failures_model->get(['failures.id_htc' => $id_htc]);
            $items['-1'] = 'TODOS';
            if($fallas != false){
                foreach($fallas as $f){
                    $items[$f->id_failure] = $f->description;
                }
            }
        }

        $this->output->set_output(json_encode($items));
        return false;
    }

    public function show_fallas($id_box){
        $this->load->model(['htcs_model','failures_model','box_failures_model']);
        $data['HEADER_MODAL']   = 'Detalles de Piezas NOK';
        $data['fallas']         = $this->box_failures_model->get_items(['box' => $id_box]);
        $this->_render_page('admin/reportes/detalle_nok_view', $data);
    }

    public function get_operadores(){
	    $id_client 		= $this->input->post('id_client');
	    $part_number 	= $this->input->post('part_number');
	    $id_operator 	= $this->input->post('id_operator');
	    $fecha_inicio   = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('f_inicio') )));
	    $fecha_fin      = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('f_final') )));

	    $indice 		= $this->input->post('indice');
	    $sql = "
	    SELECT boxes.id_operator, htcs.part_number, CONCAT_WS(' ', users.first_name, last_name, last_name_2) as operator, SUM(pieces_number) AS piezas, SUM(production_time) AS operative_time, SUM(pause_time) AS dead_time, clients.name as planta, boxes.created_on
FROM boxes
JOIN users ON users.id=boxes.id_operator
JOIN htcs ON htcs.id_htc=boxes.htc
JOIN clients ON clients.id_client=htcs.id_client
WHERE htcs.id_client='".$id_client."' AND htcs.part_number='".$part_number."' AND boxes.id_operator='".$id_operator."' AND DATE(boxes.created_on) >= '.$fecha_inicio.' AND DATE(boxes.created_on) <= '".$fecha_fin."'
GROUP BY htcs.part_number,boxes.id_operator
		";

	    $query = $this->db->query($sql);
	    $items = $query->result_array();
	    foreach($items as &$i){
		    $sql2 	= "select start_date,end_date, lunch_time from users_register_turns WHERE id_user=".$i['id_operator'] ;
		    $query2 = $this->db->query($sql2);
		    $row = $query2->row();
		    $i['lunch_time']		= $row->lunch_time;
		    $i['indice']			= $indice;
		    $i['prom']				= $i['piezas']/$indice;
		    $i['fecha']             = date('m-d-Y', strtotime($i['created_on']));
	    }

	    $this->output->set_output(json_encode($items));
        return false;
    }

    public function get_operadores_csv(){
	    $id_client 		= $this->input->post('id_client');
	    $part_number 	= $this->input->post('part_number');
	    $id_operator 	= $this->input->post('id_operator');
	    $fecha_inicio   = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('f_inicio') )));
	    $fecha_fin      = date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('f_final') )));

	    $indice 		= $this->input->post('indice');
	    $sql = "
	    SELECT boxes.id_operator, htcs.part_number, CONCAT_WS(' ', users.first_name, last_name, last_name_2) as operator, SUM(pieces_number) AS piezas, SUM(production_time) AS operative_time, SUM(pause_time) AS dead_time, clients.name as planta, boxes.created_on
FROM boxes
JOIN users ON users.id=boxes.id_operator
JOIN htcs ON htcs.id_htc=boxes.htc
JOIN clients ON clients.id_client=htcs.id_client
WHERE htcs.id_client='".$id_client."' AND htcs.part_number='".$part_number."' AND boxes.id_operator='".$id_operator."' AND DATE(boxes.created_on) >= '.$fecha_inicio.' AND DATE(boxes.created_on) <= '".$fecha_fin."'
GROUP BY htcs.part_number,boxes.id_operator
		";

	    $query = $this->db->query($sql);
	    $items = $query->result_array();


        $filename    = TMP_DIR.microtime() . '.csv';
        $fp          = fopen($filename, 'w');
        $content     = ["OPERADOR", "#DE PARTE", "CLIENTE", "PIEZAS INSPECCIONADAS" , "T. OPERATIVO" , "T. MUERTO", "T. COMIDA" , "RATE", "PZAS/HRS", "FECHA"];
        fputcsv($fp, $content);

	    foreach($items as &$i){
		    $sql2 	= "select start_date,end_date, lunch_time from users_register_turns WHERE id_user=".$i['id_operator'] ;
		    $query2 = $this->db->query($sql2);
		    $row = $query2->row();
		    $i['lunch_time']		= $row->lunch_time;
		    $i['indice']			= $indice;
		    $i['prom']				= $i['piezas']/$indice;
		    $i['fecha']             = date('m-d-Y', strtotime($i['created_on']));

		    fputcsv($fp, [$i['operator'], $i['part_number'], $i['planta'], $i['piezas'], $i['operative_time'], $i['dead_time'],  $i['lunch_time'], $i['indice'], $i['prom'], $i['fecha']]);
	    }

        fclose($fp);


        $csv = [
            'filename'  => base_url($filename),
            'status'    => 'success',
        ];

        $this->output->set_output(json_encode($csv));
        return false;
    }

    function edit_box($id){
        $this->load->model('boxes_model');
        $data['HEADER_MODAL']   = 'Editar Producción';
        $data['URL_FORM']       = base_url('apis/admin_api/update_box');
        $data['BOX']            = $this->boxes_model->get($id);

        //var_dump($data['BOX']);die;
        $this->_render_page('admin/reportes/forms/form_box_view', $data);
    }

}
