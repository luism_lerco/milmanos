<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Areas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');
    }

    public function index() {
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url('admin/login'), 'refresh');
        }
        elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
        {
            redirect(base_url('admin/inicio'), 'refresh');
        }
        else
        {
            $this->hdata = [
                'css'        => $this->load->view('admin/assets/css/datatable', '', true),
                'title'     => 'Panel Áreas',
                'breadcumb' => [
                    [
                        'url'   => base_url('admin/inicio'),
                        'label' => 'Panel'
                    ],
                    [
                        'label' => 'Configuración'
                    ],
                    [
                        'label' => 'Áreas'
                    ]
                ],
            ];

            $columns  = '"columns" : [';
            $columns .= '{ className : "v-align-middle text-center" },';
            $columns .= '{ className : "v-align-middle" },';
            $columns .= '{ className : "v-align-middle" },';
            $columns .= '{ className : "v-align-middle text-center"},';
            $columns .= '],';

            $this->js    = [
                'items'        => base_url('admin/areas/get_datatable'),
                'columns'     => $columns,
            ];

            $this->data = [
                'URL_AGREGAR'  => base_url('admin/areas/mostrar_form_area')
            ];

            $this->fdata = [
                'js'            => $this->load->view('admin/assets/js/datatable', $this->js, true),
                'link_active'    => ['#liConfiguracion', '#lkAreas']
            ];

            $this->load->view('admin/commons/header_view', $this->hdata);
            $this->load->view('admin/areas/index_view', $this->data);
            $this->load->view('admin/commons/footer_view', $this->fdata);
        }
    }

    //-----------------------------------------------------------------------------------------------------------
    public function mostrar_form_area($id = NULL){
        if (!is_null($id)) {
            $this->load->model('areas_model');
            $data['HEADER_MODAL']   = 'Editar Área';
            $data['URL_FORM']       = base_url('apis/admin_api/update_area');
            $data['AREA']           = $this->areas_model->get($id);
        } else {
            $data['HEADER_MODAL']   = 'Agregar Área';
            $data['URL_FORM']       = base_url('apis/admin_api/insert_area');
        }

        $this->_render_page('admin/areas/forms/form_area_view', $data);
    }

    public function get_datatable(){
        $modelo = 'areas_model';
        $this->load->model($modelo);

        // Datatables Variables
        $draw   = intval($this->input->get("draw"));
        $start  = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $result = $this->$modelo->get();
        $data   = [];

        foreach($result as $r) {
            $checkbox        = form_checkbox('active', 'active', $r->active, ['data-url' => base_url('apis/admin_api/update_activo/areas_model/'.$r->id_area) , 'class'=> 'chk-update-activo']);

            $operaciones     = '<a class="btn btn-info btn-xs m-b-10 show-modal" href="'.base_url('admin/areas/mostrar_form_area/'.$r->id_area).'"><i class="fa fa-pencil"></i> Editar</a>';
            //$operaciones    .= '<a class="btn btn-info btn-xs m-b-10 delete-item" data-text="planta" href="'.base_url('apis/admin_api/delete_planta/'.$r->id_plant).'"><i class="fa fa-trash"></i> Eliminar</a>';

            $data[] = [
                //'id_plant'  => $r->id_plant,
                $checkbox,
                $r->name,
                $r->description,
                $operaciones,
            ];
        }

        $output = [
            "draw"              => $draw,
            "recordsTotal"      => count($result),
            "recordsFiltered"   => count($result),
            "data"              => $data,
        ];

        $this->output->set_output(json_encode($output));
        return false;
    }

    public function _render_page($view, $data=null, $returnhtml=false)//I think this makes more sense
    {

        $this->viewdata = (empty($data)) ? $this->data: $data;
        $view_html = $this->load->view($view, $this->viewdata, $returnhtml);
        if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
    }

}
