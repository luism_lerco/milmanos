<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Operadores extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');
        date_default_timezone_set('America/Mexico_City');
    }

    public function index() {
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url('admin/login'), 'refresh');
        }
        elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
        {
            redirect(base_url('admin/inicio'), 'refresh');
        }
        else
        {
            $this->hdata = [
                'css'        => $this->load->view('admin/assets/css/datatable', '', true),
                'title'     => 'Panel Operadores',
                'breadcumb' => [
                    [
                        'url'   => base_url('admin/inicio'),
                        'label' => 'Panel'
                    ],
                    [
                        'label' => 'Operadores'
                    ]
                ],
            ];

            $columns  = '"columns" : [';
            $columns .= '{ className : "v-align-middle text-center"},';
            $columns .= '{ className : "v-align-middle" },';
            $columns .= '{ className : "v-align-middle" },';
            $columns .= '{ className : "v-align-middle" },';
            $columns .= '{ className : "v-align-middle text-center"},';
            $columns .= '],';

            $this->js    = [
                'items'        => base_url('admin/operadores/get_datatable'),
                'columns'     => $columns,
            ];

            $this->data = [
                'URL_AGREGAR'  => base_url('admin/operadores/mostrar_form_operador')
            ];

            $this->fdata = [
                'js'            => $this->load->view('admin/assets/js/datatable', $this->js, true),
                'link_active'    => ['#liOperadores']
            ];

            $this->load->view('admin/commons/header_view', $this->hdata);
            $this->load->view('admin/operadores/index_view', $this->data);
            $this->load->view('admin/commons/footer_view', $this->fdata);
        }
    }

    //-----------------------------------------------------------------------------------------------------------
    public function mostrar_form_operador($id = NULL){
        $this->load->model(['clientes_model', 'turnos_model', 'areas_model']);
        $plantas = $turnos = $areas = [];
        $tmp_p   = $this->clientes_model->get(['active' => '1']);
        $tmp_t   = $this->turnos_model->get(['active' => '1']);
        $tmp_a   = $this->areas_model->get(['active' => '1']);

        foreach($tmp_p as $p){
            $plantas[$p->id_client] = $p->name;
        }

        foreach($tmp_a as $a){
            $areas[$a->id_area] = $a->name;
        }

        foreach($tmp_t as $t){
            $turnos[$t->id_turn] = $t->name;
        }

        $data['plantas'] = $plantas;
        $data['areas']   = $areas;
        $data['turnos']  = $turnos;

        if (!is_null($id)) {
            $this->load->model('users_model');
            $data['HEADER_MODAL']   = 'Editar Operador';
            $data['URL_FORM']       = base_url('apis/admin_api/update_operador');
            $data['OPERADOR']       = $this->users_model->get($id);
        } else {
            $data['HEADER_MODAL']   = 'Agregar Operador';
            $data['URL_FORM']       = base_url('apis/admin_api/insert_operador');
        }

        $this->_render_page('admin/operadores/forms/form_operador_view', $data);
    }

    public function get_datatable(){
        $modelo = 'users_model';
        $this->load->model($modelo);

        // Datatables Variables
        $draw   = intval($this->input->get("draw"));
        $start  = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $result = $this->$modelo->get([
            'users_groups.group_id' => '2'
        ], 'users.active DESC');
        $data   = [];

        foreach($result as $r) {
            $checkbox         =  form_checkbox('active_'.$r->id, 'active', $r->active, ['data-url' => base_url('apis/admin_api/update_activo/users_model/'.$r->id) , 'class'=> 'chk-update-activo'] );
            $operaciones     = '<a class="btn btn-info btn-xs m-b-10 show-modal-lg" href="'.base_url('admin/operadores/mostrar_form_operador/'.$r->id).'"><i class="fa fa-pencil"></i> Editar</a>';

            $last_login = 'No ha accesado';

            if(!is_null($r->last_login)){
                //$last_login = ;
                $date = new DateTime(gmdate("d-m-Y H:i:s", $r->last_login), new DateTimeZone('Europe/Lisbon'));
                $date->setTimezone(new DateTimeZone('America/Mexico_City'));
                $last_login = $date->format('d-m-Y h:i:s');
            }

            $data[] = [
                $checkbox,
                $r->first_name.' '.$r->last_name.' '.$r->last_name_2,
                $r->username,
                $last_login,
                $operaciones,
            ];
        }

        $output = [
            "draw"              => $draw,
            "recordsTotal"      => count($result),
            "recordsFiltered"   => count($result),
            "data"              => $data,
        ];

        $this->output->set_output(json_encode($output));
        return false;
    }

    public function _render_page($view, $data=null, $returnhtml=false)//I think this makes more sense
    {

        $this->viewdata = (empty($data)) ? $this->data: $data;
        $view_html = $this->load->view($view, $this->viewdata, $returnhtml);
        if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
    }

}
