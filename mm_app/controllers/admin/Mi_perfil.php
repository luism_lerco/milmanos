<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mi_perfil extends API_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('users_model');
    }

    //-----------------------------------------------------------------------------------------------------------

    public function index() {
        $usuario_actual = $this->session->userdata('user_id');

        $headerData = array(
            'title' => 'Mi Perfil',
            'header' => 'Mi Perfil ',
            'breadcumb' => [
                [
                    'label' => 'Inicio',
                    'url' => base_url('admin/inicio')
                ],
                [
                    'label' => 'Mi Perfil'
                ]
            ],
        );

        $usuario = $this->users_model->get([
            'users.id'    => $usuario_actual
        ]);

        $data = array(
            'URL_ACTION'    => base_url('apis/admin_api/update_usuario'),
            'URL_SUCCESS'   => base_url('admin/inicio'),
            'URL_CANCELAR'  => base_url('admin/inicio'),
        );

        $this->fdata = [
            'active' => 'liInicio',
        ];

        if (count($usuario) > 0) {
            $data['USUARIO'] = $usuario;
        } else {
            redirect('admin/salir');
        }

        $this->load->view('admin/commons/header_view', $headerData);
        $this->load->view('admin/perfil_usuario_view', $data);
        $this->load->view('admin/commons/footer_view', $this->fdata);
    }

    //-----------------------------------------------------------------------------------------------------------
}

?>