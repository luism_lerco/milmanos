<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Htcs extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');
    }

    public function index() {
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url('admin/login'), 'refresh');
        }
        elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
        {
            redirect(base_url('admin/inicio'), 'refresh');
        }
        else
        {
            $this->hdata = [
                'css'        => $this->load->view('admin/assets/css/datatable', '', true),
                'title'     => 'Panel HI',
                'breadcumb' => [
                    [
                        'url'   => base_url('admin/inicio'),
                        'label' => 'Panel'
                    ],
                    [
                        'label' => 'HI'
                    ]
                ],
            ];

            $columns  = '"columns" : [';
            $columns .= '{ className : "v-align-middle text-center"},';
            $columns .= '{ className : "v-align-middle" },';
            $columns .= '{ className : "v-align-middle" },';
            $columns .= '{ className : "v-align-middle" },';
            $columns .= '{ className : "v-align-middle" },';
            $columns .= '{ className : "v-align-middle text-center"},';
            $columns .= '],';

            $this->js    = [
                'items'        => base_url('admin/htcs/get_datatable'),
                'columns'     => $columns,
            ];

            $this->data = [
                'URL_AGREGAR'  => base_url('admin/htcs/mostrar_form_htc')
            ];

            $this->fdata = [
                'js'            => $this->load->view('admin/assets/js/datatable', $this->js, true),
                'link_active'    => ['#liHtcs']
            ];

            $this->load->view('admin/commons/header_view', $this->hdata);
            $this->load->view('admin/htcs/index_view', $this->data);
            $this->load->view('admin/commons/footer_view', $this->fdata);
        }
    }

    //
    public function fallas($id){
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url('admin/login'), 'refresh');
        }
        elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
        {
            redirect(base_url('admin/inicio'), 'refresh');
        }
        else
        {
            $this->hdata = [
                'css'        => $this->load->view('admin/assets/css/datatable', '', true),
                'title'     => 'Panel Fallas HI',
                'breadcumb' => [
                    [
                        'url'   => base_url('admin/inicio'),
                        'label' => 'Panel'
                    ],
                    [
                        'url'   => base_url('admin/htcs'),
                        'label' => 'HI'
                    ],
                    [
                        'label' => 'Fallas HI'
                    ]
                ],
            ];

            $columns  = '"columns" : [';
            $columns .= '{ className : "v-align-middle text-center"},';
            $columns .= '{ className : "v-align-middle" },';
            $columns .= '{ className : "v-align-middle" },';
            $columns .= '{ className : "v-align-middle" },';
            $columns .= '{ className : "v-align-middle text-center"},';
            $columns .= '],';

            $this->js    = [
                'items'        => base_url('admin/htcs/get_failures_datatable/'.$id),
                'columns'     => $columns,
            ];

            $this->data = [
                'URL_AGREGAR'  => base_url('admin/htcs/mostrar_form_falla/'.$id)
            ];

            $js  = $this->load->view('admin/assets/js/datatable', $this->js, true);
            $js .= $this->load->view('admin/assets/js/fallas', '', true);

            $this->fdata = [
                'js'            => $js,
                'link_active'    => ['#liHtcs']
            ];

            $this->load->view('admin/commons/header_view', $this->hdata);
            $this->load->view('admin/htcs/fallas_view', $this->data);
            $this->load->view('admin/commons/footer_view', $this->fdata);
        }
    }

    //-----------------------------------------------------------------------------------------------------------
    public function mostrar_form_htc($id = NULL){
        $this->load->model(['clientes_model', 'proveedores_model']);
        $proveedores = $clientes = [];
        $tmp_p    = $this->clientes_model->get(['active' => '1']);
        $tmp_c    = $this->proveedores_model->get(['active' => '1']);

        foreach($tmp_c as $c){
            $proveedores[$c->id_provider] = $c->name;
        }

        foreach($tmp_p as $p){
            $clientes[$p->id_client] = $p->name;
        }

        $data['proveedores']   	= $proveedores;
        $data['clientes']    	= $clientes;

        if (!is_null($id)) {
            $this->load->model('htcs_model');
            $data['HEADER_MODAL']   = 'Editar HI';
            $data['URL_FORM']       = base_url('apis/admin_api/update_htc');
            $data['HTC']           = $this->htcs_model->get($id);
        } else {
            $data['HEADER_MODAL']   = 'Agregar HI';
            $data['URL_FORM']       = base_url('apis/admin_api/insert_htc');
        }

        $this->_render_page('admin/htcs/forms/form_htc_view', $data);
    }

    //-----------------------------------------------------------------------------------------------------------
    public function mostrar_form_falla($id_htc, $id = NULL){
        $this->load->model('htcs_model');
        $data['HTC'] = $this->htcs_model->get(['id_htc' => $id_htc]);
        if (!is_null($id)) {
            $this->load->model('failures_model');
            $data['HEADER_MODAL']   = 'Editar Falla';
            $data['URL_FORM']       = base_url('apis/admin_api/update_falla');
            $data['FALLA']          = $this->failures_model->get($id);
        } else {
            $data['HEADER_MODAL']   = 'Agregar Falla';
            $data['URL_FORM']       = base_url('apis/admin_api/insert_falla');
        }
        $this->_render_page('admin/htcs/forms/form_fallas_view', $data);
    }

    //-----------------------------------------------------------------------------------------------------------
    public function get_datatable(){
        $modelo = 'htcs_model';
        $this->load->model($modelo);

        // Datatables Variables
        $draw   = intval($this->input->get("draw"));
        $start  = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $result = $this->$modelo->get();
        $data   = [];

        foreach($result as $r) {

            $checkbox        = form_checkbox('active_'.$r->id_htc, 'active', $r->active, ['data-url' => base_url('apis/admin_api/update_activo/htcs_model/'.$r->id_htc) , 'class'=> 'chk-update-activo']);
            $operaciones     = '<a class="btn btn-info btn-xs m-b-10" href="'.base_url('admin/htcs/fallas/'.$r->id_htc).'"><i class="fa fa-exclamation-circle"></i> Fallas</a>&nbsp;';
            $operaciones    .= '<a class="btn btn-info btn-xs m-b-10 show-modal-lg" href="'.base_url('admin/htcs/mostrar_form_htc/'.$r->id_htc).'"><i class="fa fa-pencil"></i> Editar</a>';

            $data[] = [
                $checkbox,
                $r->proveedor,
                $r->cliente,
                $r->code,
                $r->part_number,
                $operaciones,
            ];
        }

        $output = [
            "draw"              => $draw,
            "recordsTotal"      => count($result),
            "recordsFiltered"   => count($result),
            "data"              => $data,
        ];

        $this->output->set_output(json_encode($output));
        return false;
    }

    public function get_failures_datatable($id){
        $modelo = 'failures_model';
        $this->load->model($modelo);

        // Datatables Variables
        $draw   = intval($this->input->get("draw"));
        $start  = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $result = $this->$modelo->get([
            'failures.id_htc' => $id
        ]);
        $data   = [];

        foreach($result as $r) {

            $img             = '<img class="img-fluid" src='.base_url(FAILURES_FOLDER.md5($r->id_htc).'/'.$r->image).' alt="'.$r->description.'"></img>';

            $checkbox        = form_checkbox('active_'.$r->id_htc, 'active', $r->active, ['data-url' => base_url('apis/admin_api/update_activo/failures_model/'.$r->id_failure) , 'class'=> 'chk-update-activo']);
            $operaciones     = '<a class="btn btn-info btn-xs m-b-10 show-modal-lg" href="'.base_url('admin/htcs/mostrar_form_falla/'.$r->id_htc.'/'.$r->id_failure).'"><i class="fa fa-pencil"></i> Editar</a>';

            $data[] = [
                $checkbox,
                $img,
                $r->part_number,
                $r->description,
                $operaciones,
            ];
        }

        $output = [
            "draw"              => $draw,
            "recordsTotal"      => count($result),
            "recordsFiltered"   => count($result),
            "data"              => $data,
        ];

        $this->output->set_output(json_encode($output));
        return false;
    }

    public function _render_page($view, $data=null, $returnhtml=false)//I think this makes more sense
    {

        $this->viewdata = (empty($data)) ? $this->data: $data;
        $view_html = $this->load->view($view, $this->viewdata, $returnhtml);
        if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
    }

}
