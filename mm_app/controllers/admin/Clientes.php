<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth');
    }

    public function index() {
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url('admin/login'), 'refresh');
        }
        elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
        {
            redirect(base_url('admin/inicio'), 'refresh');
        }
        else
        {
            $this->hdata = [
                'css'        => $this->load->view('admin/assets/css/datatable', '', true),
                'title'     => 'Panel Clientes',
                'breadcumb' => [
                    [
                        'url'   => base_url('admin/inicio'),
                        'label' => 'Panel'
                    ],
                    [
                        'label' => 'Configuración'
                    ],
                    [
                        'label' => 'Clientes'
                    ]
                ],
            ];

            $columns  = '"columns" : [';
            $columns .= '{ className : "v-align-middle text-center" },';
            $columns .= '{ className : "v-align-middle" },';
            $columns .= '{ className : "v-align-middle" },';
            $columns .= '{ className : "v-align-middle text-center"},';
            $columns .= '],';

            $this->js    = [
                'items'        => base_url('admin/clientes/get_datatable'),
                'columns'     => $columns,
            ];

            $this->data = [
                'URL_AGREGAR'  => base_url('admin/clientes/mostrar_form_cliente')
            ];

            $this->fdata = [
                'js'            => $this->load->view('admin/assets/js/datatable', $this->js, true),
                'link_active'    => ['#liConfiguracion', '#lkClientes']
            ];

            $this->load->view('admin/commons/header_view', $this->hdata);
            $this->load->view('admin/clientes/index_view', $this->data);
            $this->load->view('admin/commons/footer_view', $this->fdata);
        }
    }

    //-----------------------------------------------------------------------------------------------------------
    public function mostrar_form_cliente($id = NULL){
        if (!is_null($id)) {
            $this->load->model('clientes_model');
            $data['HEADER_MODAL']   = 'Editar Cliente';
            $data['URL_FORM']       = base_url('apis/admin_api/update_cliente');
            $data['CLIENTE']        = $this->clientes_model->get($id);
        } else {
            $data['HEADER_MODAL']   = 'Agregar Cliente';
            $data['URL_FORM']       = base_url('apis/admin_api/insert_cliente');
        }

        $this->_render_page('admin/clientes/forms/form_cliente_view', $data);
    }


    public function mostrar_contactos($id){
        if (!$this->ion_auth->logged_in())
        {
            redirect(base_url('admin/login'), 'refresh');
        }
        elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
        {
            redirect(base_url('admin/inicio'), 'refresh');
        }
        else
        {
            $this->load->model('clientes_model');

            $cliente = $this->clientes_model->get($id);

            $this->hdata = [
                'css'        => $this->load->view('admin/assets/css/datatable', '', true),
                'title'     => 'Panel Contactos de Clientes',
                'breadcumb' => [
                    [
                        'url'   => base_url('admin/inicio'),
                        'label' => 'Panel'
                    ],
                    [
                        'label' => 'Configuración'
                    ],
                    [
                        'url'   => base_url('admin/clientes'),
                        'label' => 'Clientes'
                    ],
                    [
                        'label' => 'Contactos'
                    ]
                ],
            ];

            $columns  = '"columns" : [';
            $columns .= '{ className : "v-align-middle text-center" },';
            $columns .= '{ className : "v-align-middle" },';
            $columns .= '{ className : "v-align-middle" },';
            $columns .= '{ className : "v-align-middle" },';
            $columns .= '{ className : "v-align-middle text-center"},';
            $columns .= '],';

            $this->js    = [
                'items'     => base_url('admin/clientes/get_contactos_datatable/'.$id),
                'columns'   => $columns,
            ];

            //var_dump($cliente);die;

            $this->data = [
                'cliente'       => $cliente[0]->name,
                'URL_AGREGAR'   => base_url('admin/clientes/mostrar_form_contacto/'.$id)
            ];

            $this->fdata = [
                'js'            => $this->load->view('admin/assets/js/datatable', $this->js, true),
                'link_active'    => ['#liConfiguracion', '#lkClientes']
            ];

            $this->load->view('admin/commons/header_view', $this->hdata);
            $this->load->view('admin/clientes/contactos/index_view', $this->data);
            $this->load->view('admin/commons/footer_view', $this->fdata);
        }
    }

    public function get_datatable(){
        $modelo = 'clientes_model';
        $this->load->model($modelo);

        // Datatables Variables
        $draw   = intval($this->input->get("draw"));
        $start  = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $result = $this->$modelo->get();
        $data   = [];

        foreach($result as $r) {
            $ext_num        = !is_null($r->ext_num)? ' No.Ext.'.$r->ext_num:'';
            $int_num        = !is_null($r->int_num)? 'No.Int.'.$r->ext_num:'';
            $colony         = !is_null($r->colony)?' Col.'.$r->colony:'';
            $cp             = !is_null($r->cp)? ' C.P.'.$r->cp:'';
            $municipality   = !is_null($r->municipality)?$r->municipality:'';
            $state          = !is_null($r->state)?$r->state:'';

            $checkbox         = form_checkbox('active', 'active', $r->active, ['data-url' => base_url('apis/admin_api/update_activo/clientes_model/'.$r->id_client) , 'class'=> 'chk-update-activo']);

            $operaciones     = '<a class="btn btn-info btn-xs m-b-10" href="'.base_url('admin/clientes/'.$r->id_client.'/contactos').'"><i class="fa fa-address-book-o"></i> Contactos</a> ';
            $operaciones    .= '<a class="btn btn-info btn-xs m-b-10 show-modal-lg" href="'.base_url('admin/clientes/mostrar_form_cliente/'.$r->id_client).'"><i class="fa fa-pencil"></i> Editar</a>';
            //$operaciones    .= '<a class="btn btn-info btn-xs m-b-10 delete-item" data-text="cliente" href="'.base_url('apis/admin_api/delete_cliente/'.$r->id_client).'"><i class="fa fa-trash"></i> Eliminar</a>';

            $data[] = [
                $checkbox,
                $r->name,
                trim($r->street.' '.$ext_num.' '.$int_num.' '.$colony.' '.$cp.' '.$municipality.' '.$state),
                $operaciones,
            ];
        }

        $output = [
            "draw"              => $draw,
            "recordsTotal"      => count($result),
            "recordsFiltered"   => count($result),
            "data"              => $data,
        ];

        $this->output->set_output(json_encode($output));
        return false;
    }

    public function get_contactos_datatable($id){
        $modelo = 'users_model';
        $this->load->model([$modelo,'clientes_has_contactos_model']);

        // Datatables Variables
        $draw   = intval($this->input->get("draw"));
        $start  = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $result = $this->clientes_has_contactos_model->get([
            'id_client' => $id
        ]);
        $contacts = [];

        foreach($result as $r){
            $q = $this->$modelo->get([
                'users.id' => $result[0]->id_contact
            ]);
            if($q !== FALSE){
                $tmp_contact = $this->$modelo->get([
                    'users.id'        => $q[0]->id,
                ]);

                $contacts[] = $tmp_contact[0];
            }
        }

        $data   = [];

        foreach($contacts as $r) {
            $checkbox         =  form_checkbox('active_'.$r->id, 'active', $r->active, ['data-url' => base_url('apis/admin_api/update_activo/users_model/'.$r->id) , 'class'=> 'chk-update-activo'] );
            $operaciones     = '<a class="btn btn-info btn-xs m-b-10 show-modal-lg" href="'.base_url('admin/clientes/mostrar_form_contacto/'.$id.'/'.$r->id).'"><i class="fa fa-pencil"></i> Editar</a>';

            $data[] = [
                $checkbox,
                $r->first_name.' '.$r->last_name.' '.$r->last_name_2,
                $r->username,
                gmdate("d-m-Y H:i:s", $r->last_login),
                $operaciones,
            ];
        }

        $output = [
            "draw"              => $draw,
            "recordsTotal"      => count($result),
            "recordsFiltered"   => count($result),
            "data"              => $data,
        ];

        $this->output->set_output(json_encode($output));
        return false;
    }

    //-----------------------------------------------------------------------------------------------------------
    public function mostrar_form_contacto($id_client, $id = NULL){
        $this->load->model(['users_model', 'clientes_model']);
        $data['id_cliente'] = $id_client;
        if (!is_null($id)) {
            $this->load->model('users_model');
            $data['HEADER_MODAL']   = 'Editar Contacto';
            $data['URL_FORM']       = base_url('apis/admin_api/update_contacto');
            $data['CONTACTO']       = $this->users_model->get($id);
        } else {
            $data['HEADER_MODAL']   = 'Agregar Contacto';
            $data['URL_FORM']       = base_url('apis/admin_api/insert_contacto');
        }

        $this->_render_page('admin/clientes/contactos/forms/form_contacto_view', $data);
    }

    public function _render_page($view, $data=null, $returnhtml=false)//I think this makes more sense
    {

        $this->viewdata = (empty($data)) ? $this->data: $data;
        $view_html = $this->load->view($view, $this->viewdata, $returnhtml);
        if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
    }

}
