<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    var $GID            = 2;    // ID de grupo con el que se va a comparar (2 de Operadores, 3 es de clientes)
    var $tokenExpTime   = 900;  // Tiempo de expiración en segundos
    var $margenExpTime  = 60;   // Margen de tiempo previo a expirar para generar nuevo token
    var $salt_prefix    = '';   // Version_compare(PHP_VERSION, '5.3.7', '<') ? '$2a$' : '$2y$';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['users_model', 'boxes_model', 'box_failures_model', 'failures_model', 'htcs_model', 'users_register_turns_model']);
        date_default_timezone_set('America/Mexico_City');
    }

    public function index(){
        show_404();
    }

    public function login()
    {
        $result = [];

        $username     = $this->input->get_post('usuario',true);
        $password     = $this->input->get_post('password',true);

        /* --- INI Validaciones negativas ---- */
        if($username == '' || $password == ''){
            $result['s']    = false;
            $result['er']   = 1;
            $result['m']    = 'Campos incompletos';
            echo json_encode($result);
            return false;
        }

        $userfront = $this->users_model->getUserbyUsername($username, $this->GID);
        //echo '<pre>'; print_r($userfront); die;
        if(empty($userfront)){
            $result['s'] = false;
            $result['er'] = 3;
            $result['m'] = 'Usuario y/o contraseña no válidos';
            echo json_encode($result);
            return false;
        }

        if( !$this->bcrypt->verify($password,$userfront->password) ){
            $result['s']    = false;
            $result['er']   = 4;
            $result['m']    = 'Usuario y/o contraseña no válidos';
            echo json_encode($result);
            return false;
        }
        /* --- END Validaciones negativas ---- */



        //para no enviar a front esta info
        unset($userfront->password);

        $tokenData = $this->users_model->getWSToken($userfront->uid);

        if( empty($tokenData) ){
            $this->users_model->setWSToken($userfront->uid);
            $tokenData = $this->users_model->getWSToken($userfront->uid);
        }

        //no se generó el token
        if( !isset($tokenData->key) || empty($tokenData->key) ){
            $result['s'] = false;
            $result['m'] = 'No se generó token';
            echo json_encode($result);
            return false;
        }
        //ya expiró el token?
        if( ($tokenData->time + $this->tokenExpTime ) <=  (time() - $this->margenExpTime) ){
            $this->users_model->updateWSToken($userfront->uid);
            $tokenData = $this->users_model->getWSToken($userfront->uid);
        }

        $this->users_model->update(['last_login' => time() ], $userfront->uid);

        $result['s'] = true;
        $result['m'] = 'Login realizado correctamente';
        $result['k'] = $tokenData->key;
        $result['u'] = $userfront;
        echo json_encode($result);
    }

    public function get_production(){
        $result = [];

        $uid = $this->input->get_post('u',true); // user ID
        $tk  = $this->input->get_post('k',true); // token key

        // no UID
        if($uid === false || $tk == false || !($uid)>0 || $tk=='' ){
            $result['s'] = false;
            echo json_encode($result);
            return false;
        }

        $tokenData = $this->users_model->getWSToken($uid);

        if(empty($tokenData)){
            $result['s'] = false;
            $result['m'] = 'No se ha recibido el datos de token';
            echo json_encode($result);
            return false;
        }

        if( !isset($tokenData->key) || $tokenData->key!= $tk ){
            $result['s'] = false;
            $result['m'] = 'El token ha caducado';
            echo json_encode($result);
            return false;
        }

        //ya expiró el token?
        if( ($tokenData->time + $this->tokenExpTime ) <=  (time() - $this->margenExpTime) ){
            $this->users_model->updateWSToken($uid);
            $tokenData = $this->users_model->getWSToken($uid);
        }

        $userfront = $this->users_model->getUserbyID($uid, $this->GID);
        //echo '<pre>'; print_r($userfront); die;
        if(empty($userfront)){
            $result['s'] = false;
            $result['er'] = 3;
            $result['m'] = 'Usuario y/o contraseña no válidos';
            echo json_encode($result);
            return false;
        }



        $htcs = $this->htcs_model->get([
            'htcs.id_client' => $userfront->id_plant,
            'htcs.active'   => '1'
        ]);

        $lotes = [];

        if(is_array($htcs) && !empty($htcs) ){
            foreach($htcs as $htc){
                $tmp_f = $this->failures_model->get([
                    'failures.id_htc' => $htc->id_htc,
                    'failures.active' => '1'
                ]);

                if(!is_array($tmp_f) && empty($tmp_f) ){
                    $result['s'] = false;
                    $result['m'] = 'No se encontraron imágenes de fallas asignadas al número de pieza: '.$lot->part_number;
                    $result['k'] = $tokenData->key;
                    echo json_encode($result);
                    return false;
                }

                $failures = [];
                foreach($tmp_f as $f){
                    $t_f = [];
                    $t_f['id_failure']  = $f->id_failure;
                    $t_f['id_lot']      = $f->id_htc;
                    $t_f['image']       = base_url('assets/failures/'.md5($f->id_htc).'/'.$f->image);
                    $t_f['description'] = $f->description;
                    $t_f['created_on']  = $f->created_on;
                    $t_f['active']      = $f->active;
                    $failures[]         = (Object) $t_f;
                }

                $h  = [];
                $h['id_lot']        = $htc->id_htc;
                $h['id_plant']      = $htc->id_client;
                $h['id_client']     = $htc->id_provider;
                $h['code']          = $htc->code;
                $h['part_number']   = $htc->part_number;
                $h['created_on']    = $htc->created_on;
                $h['active']        = $htc->active;
                $h['cliente']       = $htc->proveedor;
                $h['planta']        = $htc->cliente;

                $tmp_l           = (Object) $h;
                $tmp_l->failures = (Object) $failures;

                $lotes[] = $tmp_l;
            }

            $result['s'] = true;
            $result['m'] = 'Se obtuvo número de parte de producción correctamente';
            $result['k'] = $tokenData->key;
            $result['i'] = $lotes;
            echo json_encode($result);
            return false;
        }

        $result['s'] = false;
        $result['m'] = 'No se encontraron cajas asignadas al operador pendientes de producción.';
        $result['k'] = $tokenData->key;
        echo json_encode($result);
    }

    public function update_production(){
        $result = [];

        $uid    = $this->input->get_post('u',true); // user ID
        $tk     = $this->input->get_post('k',true); // token key
        // no UID
        if($uid === false || $tk == false || !($uid)>0 || $tk=='' ){
            $result['s'] = false;
            echo json_encode($result);
            return false;
        }

        $tokenData = $this->users_model->getWSToken($uid);

        /*if(empty($tokenData)){
            $result['s'] = false;
            echo json_encode($result);
            return false;
        }

        if( !isset($tokenData->key) || $tokenData->key!= $tk ){
            $result['s'] = false;
            echo json_encode($result);
            return false;
        }*/

        $this->users_model->updateWSToken($uid);

        /*//ya expiró el token?
        if( ($tokenData->time + $this->tokenExpTime ) <=  (time() - $this->margenExpTime) ){
            $this->users_model->updateWSToken($uid);
            $tokenData = $this->users_model->getWSToken($uid);
        }*/

        $id_htc = $this->input->get_post('b', true); // lot ID

        $box                    = [];
        $box['htc']		        = $id_htc;
        $box['id_operator']     = $uid;
        $box['lote']			= $this->input->get_post('lt',true);
        $box['serial']          = $this->input->get_post('ns',true);
        $box['nok_pieces']      = $this->input->get_post('nok',true);
        $box['production_time'] = $this->input->get_post('pt',true);
        $box['pause_time']      = $this->input->get_post('ps',true);
        $box['pieces_number']   = $this->input->get_post('tp',true);
        $box['box_number']      = $this->input->get_post('bn',true);
        $box['created_on']      = date("Y-m-d H:i:s");
        $box['complete']        = 1;
        $id_box = $this->boxes_model->insert($box);

        $tmp_f      = $this->input->get_post('f', true);
        $failures   = json_decode($tmp_f, true);
        foreach($failures as $index => $f){
            $fail               = [];
            $fail['box']        = $id_box;
            $fail['id_failure'] = $index;
            $fail['nok_pieces'] = $f;
            $this->box_failures_model->insert($fail);
        }

        $result['s'] = true;
        $result['m'] = 'Se agrego la caja a producción correctamente';
        $result['k'] = $tokenData->key;
        echo json_encode($result);
        return false;
    }

    public function register_turn(){
        $result = [];

        $uid    = $this->input->get_post('u',true);     // user ID
        $tk     = $this->input->get_post('k',true);     // token key

        // no UID
        if($uid === false || $tk == false || !($uid)>0 || $tk=='' ){
            $result['s'] = false;
            echo json_encode($result);
            return false;
        }

        $tokenData = $this->users_model->getWSToken($uid);

        if(empty($tokenData)){
            $result['s'] = false;
            echo json_encode($result);
            return false;
        }

        if( !isset($tokenData->key) || $tokenData->key!= $tk ){
            $result['s'] = false;
            echo json_encode($result);
            return false;
        }

        //ya expiró el token?
        if( ($tokenData->time + $this->tokenExpTime ) <=  (time() - $this->margenExpTime) ){
            $this->users_model->updateWSToken($uid);
            $tokenData = $this->users_model->getWSToken($uid);
        }

        $sd     = $this->input->get_post('sd', true);   // Start Date
        $ed     = $this->input->get_post('ed', true);   // End Date
        $lt     = $this->input->get_post('lt', true);   //Lunch Time

        $turn                   = [];
        $turn['id_user']        = $uid;
        $turn['start_date']     = strtotime($sd);
        $turn['end_date']       = strtotime($ed);
        $turn['lunch_time']     = intval($lt);

        $id_turn = $this->users_register_turns_model->insert($turn);

        $result['s'] = true;
        $result['m'] = 'Se agrego la jornada del operador correctamente';
        $result['k'] = $tokenData->key;
        echo json_encode($result);
        return false;
    }
}
