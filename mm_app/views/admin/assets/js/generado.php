<script src="<?=base_url('assets/plugins/nvd3/lib/d3.v3.js')?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/nvd3/nv.d3.min.js')?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/nvd3/src/utils.js')?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/nvd3/src/tooltip.js')?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/nvd3/src/interactiveLayer.js')?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/nvd3/src/models/axis.js')?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/nvd3/src/models/line.js')?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/nvd3/src/models/lineWithFocusChart.js')?>" type="text/javascript"></script>
<script src="<?=base_url('assets/plugins/jquery-sparkline/jquery.sparkline.min.js')?>" type="text/javascript"></script>
<script>
    nv.addGraph(function() {
        var chart = nv.models.pieChart()
        .x(function(d) { return d.label })
        .y(function(d) { return d.value })
        .showLabels(true);

        d3.select("#nvd3-pie svg")
            .datum([
            {
                "label": "Piezas OK",
                "value" : <?=($t_ok > 0)? $t_ok:0;?>
            } ,
            {
                "label": "Piezas NOK",
                "value" : <?=$t_nok?>
            }
        ]).transition().duration(350).call(chart);
        return chart;
    });

    $(document).on('click', '#generate_csv', function(e){
        e.preventDefault();
        var token   = $(this).data('token');

        $.ajax({
            type: "GET",
            url: "<?=base_url('admin/reportes/generado_csv/')?>"+token,
            success: function(data){
                var opts = $.parseJSON(data);
                if(opts.status == 'success'){
                    window.location.href = opts.filename;
                }
                console.log(opts);
            }
        });
    });
</script>