<script src="<?=base_url('assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js')?>"></script>
<script type="text/javascript">
    $('#myModal').on('shown.bs.modal', function (e) {
        $('#time_start').timepicker();
        $('#time_end').timepicker();
    });
</script>