<script type="text/javascript" src="<?=base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js')?>" charset="UTF-8"></script>
<script type="text/javascript">
    $('#myBigModal').on('shown.bs.modal', function() {
        $('.input-group.date').datepicker({
            format: 'dd/mm/yyyy',
            language: "es"
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.input-group.date').datepicker({
            format: 'dd/mm/yyyy',
            language: "es"
        });
    });

    $(document).on('change', '#id_client', function(){
        var id_client = $(this).val();
        $.ajax({
            type: "POST",
            url: "<?=base_url('admin/reportes/get_serial_numbers')?>",
            data: { 'id_client': id_client  },
            success: function(data){
                var opts = $.parseJSON(data);
                $('#part_number').html('');
                $.each(opts, function(i, v) {
                    if(i=='-1'){
                        $('#part_number').append('<option value="' + i + '" disabled selected>' + v + '</option>');
                    }else{
                        $('#part_number').append('<option value="' + i + '">' + v + '</option>');
                    }
                });
            }
        });
    });

    $(document).on('change', '#part_number', function(){
        var id_client   = $('#id_client').val();
        var part_number = $(this).val();

        $.ajax({
            type: "POST",
            url: "<?=base_url('admin/reportes/get_proveedores')?>",
            data: { 'part_number': part_number, 'id_client': id_client },
            success: function(data){
                var opts = $.parseJSON(data);
                $('#id_provider').html('');
                $.each(opts, function(i, v) {
                    if(i=='-1'){
                        $('#id_provider').append('<option value="' + i + '" disabled selected>' + v + '</option>');
                    }else{
                        $('#id_provider').append('<option value="' + i + '">' + v + '</option>');
                    }
                });
            }
        });
    });

    $(document).on('change', '#id_provider', function(){
        var id_client   = $('#id_client').val();
        var part_number = $('#part_number').val();
        var id_provider = $(this).val();

        $.ajax({
            type: "POST",
            url: "<?=base_url('admin/reportes/get_fallas')?>",
            data: { 'part_number': part_number, 'id_client': id_client, 'id_provider' : id_provider },
            success: function(data){
                var opts = $.parseJSON(data);
                $('#defectos').html('');
                $.each(opts, function(i, v) {
                    $('#defectos').append('<option value="' + i + '">' + v + '</option>');
                });
            }
        });
    });

    function mostrar_reporte(info){
        console.log(info);
        window.location.href = "<?=base_url('admin/reportes/generado')?>/"+info.no_reporte;
    }
</script>