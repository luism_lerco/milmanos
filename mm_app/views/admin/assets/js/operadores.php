<script type="text/javascript" src="<?=base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js')?>" charset="UTF-8"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.input-group.date').datepicker({
            format: 'dd/mm/yyyy',
            language: "es"
        });
    });

    $(document).on('change', '#id_client', function(){
	    var id_client = $(this).val();
        $.ajax({
            type: "POST",
            url: "<?=base_url('admin/reportes/get_serial_numbers')?>",
            data: { 'id_client': id_client  },
            success: function(data){
                var opts = $.parseJSON(data);
                $('#part_number').html('');
                $.each(opts, function(i, v) {
                    if(i=='-1'){
                        $('#part_number').append('<option value="' + i + '" disabled selected>' + v + '</option>');
                    }else{
                        $('#part_number').append('<option value="' + i + '">' + v + '</option>');
                    }
                });
            }
        });
    });

    $(document).on('change', '#part_number', function(){
		var part_number = $(this).val();
        $.ajax({
            type: "POST",
            url: "<?=base_url('admin/reportes/get_operators')?>",
            data: { 'part_number': part_number  },
            success: function(data){
                var opts = $.parseJSON(data);
                $('#id_operator').html('');
                $.each(opts, function(i, v) {
                    if(i=='-1'){
                        $('#id_operator').append('<option value="' + i + '" disabled selected>' + v + '</option>');
                    }else{
                        $('#id_operator').append('<option value="' + i + '">' + v + '</option>');
                    }
                });
            }
        });
    });

    $(document).on('click', '#generar', function(){
	    var id_client 	= $('#id_client').val();
	    var part_number = $('#part_number').val();
	    var id_operator	= $('#id_operator').val();
	    var f_inicio    = $('#fecha_inicio').val();
	    var f_final     = $('#fecha_fin').val();
	    var indice 		= $('#indice').val();
	    $.ajax({
            type: "POST",
            url: "<?=base_url('admin/reportes/get_operadores')?>",
            data: {id_client:id_client, part_number:part_number, id_operator:id_operator, indice: indice, f_inicio: f_inicio, f_final: f_final },
            success: function(data){
                $('#generate_operadores_csv').attr('data-id_client', id_client);
                $('#generate_operadores_csv').attr('data-part_number', part_number);
                $('#generate_operadores_csv').attr('data-id_operator', id_operator);
                $('#generate_operadores_csv').attr('data-f_inicio', f_inicio);
                $('#generate_operadores_csv').attr('data-f_final', f_final);
                $('#generate_operadores_csv').attr('data-indice', indice);
                $('#data-operador').show();
                $('#generate_operadores_csv').show();
                var opts = $.parseJSON(data);
                $('#data-operador tbody').html('');
                $.each(opts, function(k, v) {
                    $('#data-operador tbody').append('<tr><td class="">'+v.operator+'</td><td class="text-center">'+v.part_number+'</td><td class="text-center">'+v.planta+'</td><td class="text-center">'+v.piezas+'</td><td class="text-center">'+v.operative_time+' mins.</td><td class="text-center">'+v.dead_time+' mins.</td><td class="text-center">'+v.lunch_time+' mins.</td><td class="text-center">'+v.indice+'</td><td class="text-center">'+v.prom+'</td><td class="text-center">'+v.fecha+'</td></tr>');
                });
            }
        });
    });

    $(document).on('click', '#generate_operadores_csv', function(e){
        e.preventDefault();
        var id_client   = $(this).data('id_client');
	    var part_number = $(this).data('part_number');
	    var id_operator	= $(this).data('id_operator');
	    var f_inicio    = $(this).data('f_inicio');
	    var f_final     = $(this).data('f_final');
	    var indice 		= $(this).data('indice');


        $.ajax({
            type: "POST",
            url: "<?=base_url('admin/reportes/get_operadores_csv')?>",
            data: {id_client:id_client, part_number:part_number, id_operator:id_operator, indice: indice, f_inicio: f_inicio, f_final: f_final },
            success: function(data){
                var opts = $.parseJSON(data);
                if(opts.status == 'success'){
                    window.location.href = opts.filename;
                }
                console.log(opts);
            }
        });
    });
</script>