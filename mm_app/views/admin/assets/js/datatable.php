<script type="text/javascript" src="<?=base_url('assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/plugins/datatables-responsive/js/datatables.responsive.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/plugins/datatables-responsive/js/lodash.min.js')?>"></script>

<script type="text/javascript">
    var table;
    $(document).ready(function() {
        table = $('#datatable').DataTable({
            "ajax": {
                url : "<?=$items?>",
                type : 'GET'
            },
            "sDom": "<'exportOptions'T><'table-responsive't><'row'<p i>>",
            "sPaginationType": "bootstrap",
            "destroy": true,
            language: {
            url: '//cdn.datatables.net/plug-ins/1.10.7/i18n/Spanish.json'
            },
            <?=isset($columns)?$columns:''?>
            "iDisplayLength": 5,
            "oTableTools": {
                "sSwfPath": "<?=base_url('assets/plugins/jquery-datatable/extensions/TableTools/swf/copy_csv_xls_pdf.swf')?>    ",
                "aButtons": [{
                    "sExtends": "csv",
                    "sButtonText": "<i class='pg-grid'></i>",
                }, {
                    "sExtends": "xls",
                    "sButtonText": "<i class='fa fa-file-excel-o'></i>",
                }, {
                    "sExtends": "pdf",
                    "sButtonText": "<i class='fa fa-file-pdf-o'></i>",
                }, {
                    "sExtends": "copy",
                    "sButtonText": "<i class='fa fa-copy'></i>",
                }]
            },
            fnDrawCallback: function(oSettings) {
                $('.export-options-container').append($('.exportOptions'));
            }
        });
    });

    function refresh_datatable(){
        showSubLoader('Espera un momento...');
        table.ajax.reload();
        closeSubLoader();
    }

    $(document).on('click', '#panel-table-refresh', function(){
        refresh_datatable();
    });

    $(document).on("click", ".delete", function (e) {
        e.preventDefault();
        var self = $(this);
        var text = $(this).attr('data-text');
        showConfirm('Eliminar elemento', '¿Deseas eliminar este '+ text +' ?', function () {
            var url = self.attr('href');
            console.log(url);
            showSubLoader('Espera un momento...');
            $.post(url, {
            }, function (o) {
                $('#divConfirm').modal('hide');
                refresh_datatable();
                closeSubLoader();
            }, 'json');

            return  false;
        });
    });
</script>