<div class="card card-transparent">
    <div class="card-header ">
        <div class="card-title">Lista de Fallas HI</div>
        <div class="pull-right">
            <div class="col-xs-12">
                <a href="<?= $URL_AGREGAR ?>" class="btn btn-primary btn-cons show-modal-lg"><i class="fa fa-plus"></i> Nueva Falla</a>
            </div>
        </div>
    </div>
    <div class="card-block">
        <div class="table-responsive">
            <div id="basicTable_wrapper" class="dataTables_wrapper no-footer">
                <div class="export-options-container"></div>
                <table id="datatable" class="table table-hover no-footer" cellspacing="0" style="width:100%">
                    <thead>
                        <tr>
                            <th class="text-center">Activo</th>
                            <th>Imagen</th>
                            <th>No. Parte</th>
                            <th>Descripción</th>
                            <th class="text-right">Operaciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>