<div class="modal-header clearfix text-left">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
</button>
    <h5><?= $HEADER_MODAL ?></h5>
</div>
<form id="upload-file"  method="post" action="<?=base_url('apis/admin_api/upload_tmp_image')?>" class="ajaxPostForm"  style="display:none" data-function-success="recargar_documento">
    <input type="file" id="tmp-image" name="image" accept="image/*" />
    <input type="submit">
</form>
<div id="file-uploader" data-id="" data-target="" class="hidden"></div>
<form role="form" method="post" action="<?= $URL_FORM ?>" class="ajaxPostFormModal" data-function-success="refresh_datatable">
    <div class="modal-body">
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label for="image">Imagen *</label>
                    <input id="image" name="image" type="hidden" value="<?= (isset($FALLA) ? $FALLA[0]->image : '') ?>"/>
                <p class="text-center">
                    <a href="#" class="upload-image">
                        <img id="img_image" class="img-responsive img-thumbnail" src="<?php
                        if (isset($FALLA) && $FALLA[0]->image != '')
                            echo base_url(FAILURES_FOLDER.md5($FALLA[0]->id_htc).'/'.$FALLA[0]->image);
                        else
                            echo 'http://placehold.it/1200x800';
                        ?>"/>
                    </a>
                </p>
                </div>
            </div>
            <div class="col-md-7">
                <div class="form-group">
                    <label for="part-number">No. Parte</label>
                    <input type="text" readonly class="form-control-plaintext" id="part-number" value="<?=$HTC[0]->part_number?>">
                </div>
                <div class="form-group">
                    <label for="description">Descripción *</label>
                    <textarea name="description" class="form-control" rows="5" placeholder="Descripción"><?= (isset($FALLA) ? $FALLA[0]->description : '') ?></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <input type="hidden" name="id_lot" value="<?=$HTC[0]->id_htc?>"/>
        <input type="hidden" name="id" value="<?= (isset($FALLA) ? $FALLA[0]->id_failure : '') ?>"/>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</form>