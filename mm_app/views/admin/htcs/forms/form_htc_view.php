<div class="modal-header clearfix text-left">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
</button>
    <h5><?= $HEADER_MODAL ?></h5>
</div>
<div id="file-uploader" class="hidden"></div>
<div id="file-uploader-movil" class="hidden"></div>
<form role="form" method="post" action="<?= $URL_FORM ?>" class="ajaxPostFormModal" data-function-success="refresh_datatable">
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="id_client">Proveedor *</label>
                    <?php $id_client = isset($HTC) ? $HTC[0]->id_provider : '';?>
                    <?php echo form_dropdown('id_client', $proveedores , $id_client, ['class' => 'form-control' ]); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="id_plant">Cliente *</label>
                    <?php $id_plant = isset($HTC) ? $HTC[0]->id_client : '';?>
                    <?php echo form_dropdown('id_plant', $clientes, $id_plant, ['class' => 'form-control' ]); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="code">Código *</label>
                    <input name="code" class="form-control" placeholder="Código" value="<?= (isset($HTC) ? $HTC[0]->code : '') ?>"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="part_number">No. Parte</label>
                    <input name="part_number" class="form-control" placeholder="No. Parte" value="<?= (isset($HTC) ? $HTC[0]->part_number : '') ?>"/>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <input type="hidden" name="id" value="<?= (isset($HTC) ? $HTC[0]->id_htc : '') ?>"/>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</form>