<div class="modal-header clearfix text-left">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
</button>
    <h5><?= $HEADER_MODAL ?></h5>
</div>
<div id="file-uploader" class="hidden"></div>
<div id="file-uploader-movil" class="hidden"></div>
<form role="form" method="post" action="<?= $URL_FORM ?>" class="ajaxPostFormModal" data-function-success="refresh_datatable">
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="proveedor">Proveedor</label>
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="<?= (isset($BOX) ? $BOX[0]->provider : '') ?>">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="box_number">No. de Caja *</label>
                    <input name="box_number" class="form-control"
                           placeholder="No. de Caja" value="<?= (isset($BOX) ? $BOX[0]->box_number : '') ?>"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="pieces_number">No. de Piezas *</label>
                    <input name="pieces_number" class="form-control"
                           placeholder="No. Piezas" value="<?= (isset($BOX) ? $BOX[0]->pieces_number : '') ?>"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="lote">Lote </label>
                    <input name="lote" class="form-control"
                           placeholder="Lote" value="<?= (isset($BOX) ? $BOX[0]->lote : '') ?>"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="serial">Serial </label>
                    <input name="serial" class="form-control"
                           placeholder="Serial" value="<?= (isset($BOX) ? $BOX[0]->serial : '') ?>"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="nok_pieces">Piezas NO OK *</label>
                    <input name="nok_pieces" class="form-control"
                           placeholder="Piezas NO OK" value="<?= (isset($BOX) ? $BOX[0]->nok_pieces : '') ?>"/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="production_time">T. de Producción *</label>
                    <input name="production_time" class="form-control"
                           placeholder="Tiempo de Producción" value="<?= (isset($BOX) ? $BOX[0]->production_time : '') ?>"/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="pause_time">T. de Pausa *</label>
                    <input name="pause_time" class="form-control"
                           placeholder="Tiempo de Pausa" value="<?= (isset($BOX) ? $BOX[0]->pause_time : '') ?>"/>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <input type="hidden" name="id" value="<?= (isset($BOX) ? $BOX[0]->id_box : '') ?>"/>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</form>