<div class="modal-header clearfix text-left">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
</button>
    <h5><?= $HEADER_MODAL?></h5>
</div>
<div class="modal-body">
    <?php if(isset($fallas) && (is_array($fallas) || is_object($fallas) ) && !empty($fallas)){?>
        <?php foreach($fallas as $f){?>
        <div class="row">
            <div class="col-md-4">
                <img class="img-fluid" src='<?=base_url(FAILURES_FOLDER.md5($f->id_htc).'/'.$f->image)?>' alt="<?=$f->description?>"></img>
            </div>
            <div class="col-md-4">
                <?=$f->description?>
            </div>
            <div class="col-md-4">
                <?=$f->nok_pieces?>
            </div>
        </div>
        <?php } ?>
    <?php }?>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
</div>