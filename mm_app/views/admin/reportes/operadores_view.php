<div class="invoice padding-50 sm-padding-10">
    <div>
        <div>
            <div class="pull-left">
                <img width="198" height="60" alt="" class="invoice-logo" data-src-retina="<?=base_url('assets/img/logo_dashboard.png')?>" data-src="<?=base_url('assets/img/logo_dashboard.png')?>" src="<?=base_url('assets/img/logo_dashboard.png')?>">
            </div>
            <div class="pull-right sm-m-t-20">
                <h2 class="font-montserrat all-caps hint-text">Mil Manos</h2>
            </div>
        </div>
        <br>
        <br>
        <div class="card card-default no-print">
            <div class="card-body p-l-10 p-r-10">
                <h3>Configuración de Reporte de Operadores</h3>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="id_client">Cliente</label>
                            <?=form_dropdown('id_client', $clientes, [], ['class' => 'form-control', 'id' => 'id_client']);?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="part_number">No. Parte</label>
                            <select id="part_number" name="part_number" class="form-control">
                                <option value="-1" selected disabled>-- seleccione --</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="id_operator">Operador</label>
                            <select id="id_operator" name="id_operator" class="form-control">
                                <option value="-1" selected disabled>-- seleccione --</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="fecha_inicio">Fecha Inicio *</label>
                            <div class="input-group date col-md-12 p-l-0">
                                <input type="text" id="fecha_inicio" name="fecha_inicio" class="form-control" value="<?=date('d/m/Y')?>">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="fecha_fin">Fecha Fin *</label>
                            <div class="input-group date col-md-12 p-l-0">
                                <input type="text" id="fecha_fin" name="fecha_fin" class="form-control" value="<?=date('d/m/Y')?>">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>Rate</label>
                            <input type="text" id="indice" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <div class="form-group m-t-30">
                            <button class="btn btn-primary" id="generar">Generar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <?php if($this->ion_auth->is_admin()):?>
        <div class="pull-right">
            <a id="generate_operadores_csv" class="btn btn-primary" href="<?=base_url('')?>" style="display:none"><i class="fa fa-download"></i> CSV</a>
        </div>
        <?php endif;?>
        <br>
        <div class="table-responsive table-invoice">
            <table id="data-operador" class="table m-t-50" style="display:none">
                 <thead>
                    <tr>
                        <th class="">Operador</th>
                        <th class="text-center">#de Parte</th>
                        <th class="text-center">Cliente</th>
                        <th class="text-center">Piezas Inspeccionadas</th>
                        <th class="text-right">T. operativo</th>
                        <th class="text-right">T. muerto</th>
                        <th class="text-right">T. comida</th>
                        <th class="text-center">Rate</th>
                        <th class="text-center">Pzas/Hrs</th>
                        <th class="text-center">Fecha</th>
                    </tr>
                  </thead>
                  <tbody>
                 </tbody>
            </table>
        </div>
    <hr>
</div>