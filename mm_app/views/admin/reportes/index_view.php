<div class="card card-transparent">
    <div class="card-header ">
        <div class="card-title">Producción Actual Cajas [ Reporte ]</div>
        <div class="pull-right">
            <div class="col-xs-12">
                <a href="<?= $URL_REPORTE ?>" class="btn btn-primary btn-cons show-modal-lg"><i class="fa fa-plus"></i> Generar Reporte</a>
            </div>
        </div>
    </div>
    <div class="card-block">
        <div class="table-responsive">
            <div id="basicTable_wrapper" class="dataTables_wrapper no-footer">
                <div class="export-options-container"></div>
                <table id="datatable" class="table table-hover no-footer" cellspacing="0" style="width:100%">
                    <thead>
                        <tr>
                            <th>Proveedor</th>
                            <th>No. Caja</th>
                            <th>No.Piezas</th>
                            <th>Lote</th>
                            <th>Serial</th>
                            <th>Piezas No OK</th>
                            <?php if($this->ion_auth->is_admin()):?>
                            <th>T.Producción</th>
                            <th>T.Pausa</th>
                            <th>Fecha</th>
                            <th>Acciones</th>
                            <?php endif;?>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>