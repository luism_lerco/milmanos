<div class="modal-header clearfix text-left">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
</button>
    <h5><?= $HEADER_MODAL?></h5>
</div>
<div id="file-uploader" class="hidden"></div>
<div id="file-uploader-movil" class="hidden"></div>
<form role="form" method="post" action="<?= $URL_FORM ?>" class="ajaxPostFormModal" data-function-success="mostrar_reporte">
    <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="part_number">Cliente</label>
                    <?php echo form_dropdown('id_client', $clientes, '', ['id'=>'id_client','class' => 'form-control' ]); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="part_number">No. Parte</label>
                    <select id="part_number" name="part_number" class="form-control">
                        <option value="-1" disabled selected>-- seleccione --</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="id_provider">Proveedor *</label>
                    <select id="id_provider" name="id_provider" class="form-control">
                        <option value="-1" disabled selected>-- seleccione --</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="fecha_inicio">Fecha Inicio *</label>
                    <div class="input-group date col-md-12 p-l-0">
                        <input type="text" name="fecha_inicio" class="form-control" value="<?=date('d-m-Y')?>">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="fecha_fin">Fecha Fin *</label>
                    <div class="input-group date col-md-12 p-l-0">
                        <input type="text" name="fecha_fin" class="form-control" value="<?=date('d-m-Y')?>">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="defectos">Defectos *</label>
                    <select id="defectos" name="defectos" class="form-control">
                        <option value="-1" disabled selected>-- seleccione --</option>
                    </select>
                </div>
            </div>

        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Generar</button>
    </div>
</form>