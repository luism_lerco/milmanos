<div class="card card-default">
    <div class="card-header no-print">
        <div class="pull-right">
            <div class="col-xs-12">
                <button id="generate_csv" class="btn btn-primary btn-cons" data-token="<?=$token?>"><i class="fa fa-download"></i> CSV</button>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="card-body">
        <div class="invoice padding-50 sm-padding-10">
            <div>
                <div class="pull-left">
                    <img width="198" height="60" alt="" class="invoice-logo" data-src-retina="<?=base_url('assets/img/logo_dashboard.png')?>" data-src="<?=base_url('assets/img/logo_dashboard.png')?>" src="<?=base_url('assets/img/logo_dashboard.png')?>">
                </div>
                <div class="pull-right sm-m-t-20">
                    <h2 class="font-montserrat all-caps hint-text"><?=$cliente->name?></h2>
                </div>
            </div>
            <br>
            <br>
            <div>
                <div class="pull-left">
                    <div>
                        <div class="pull-left">
                            <div class="font-montserrat bold all-caps">No. Parte: </div>
                            <div class="font-montserrat bold all-caps">Proveedor: </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="pull-right">
                            <div class=""><?=$part_number?></div>
                            <div class=""><?=$proveedor->name?></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="pull-right">
                    <div>
                        <div class="pull-left">
                            <div class="font-montserrat bold all-caps">Fecha Inicio: </div>
                            <div class="font-montserrat bold all-caps">Fecha Final: </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="pull-right">
                            <div class=""><?=$fecha_inicio?></div>
                            <div class=""><?=$fecha_fin?></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive table-invoice">
                <table class="table m-t-50">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="">No.Parte</th>
                            <th class="text-center">Lote</th>
                            <th class="text-center">Serial</th>
                            <th class="text-center">Pzas OK</th>
                            <th class="text-center">Pzas No OK</th>
                            <th class="text-center">Pzas Inspeccionadas</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php if(isset($fallas) && (is_array($fallas) || is_object($fallas)) && !empty($fallas) && (count($fallas) > 0) ):?>
                        <?php foreach($fallas as $d):?>
                        <tr>
                            <td class="text-center"><?=$i?></td>
                            <td class=""><p class="text-black"><?=$d['part_number']?></p></td>
                            <td class=""><?=$d['lote']?></td>
                            <td class="text-center"><?=$d['serial']?></td>
                            <td class="text-center"><?=$d['pzas_caja']-$d['pzas_nok']?></td>
                            <td class="text-center"><?=$d['pzas_nok']?></td>
                            <td class="text-center"><?=$d['pzas_caja']?></td>
                        </tr>
                        <?php $i++; ?>
                        <?php endforeach;?>
                        <?php else:?>
                        <tr>
                            <td colspan="7" class="text-center">No se encontraron registros</td>
                        </tr>
                        <?php endif;?>
                    </tbody>
                </table>
            </div>
            <br>
            <div class="col-12">
                <div class="row">
                    <div class="col-lg-7 sm-no-padding sm-p-b-20 d-flex align-items-end justify-content-between">
                        <div id="nvd3-pie" class="line-chart m-t-30 text-center" data-x-grid="false">
                          <svg style="height:300px;width:500px"></svg>
                        </div>
                    </div>
                    <div class="col-lg-5 sm-no-padding sm-p-b-20 d-flex align-items-end justify-content-between">
                        <table class="table table-invoice">
                            <tr>
                                <th colspan="2">TOTALES</th>
                            </tr>
                            <tr>
                                <td><b>Pzas Inspeccionadas</b></td>
                                <td><?=$t_pzas?></td>
                            </tr>
                            <tr>
                                <td><b>Pzas OK</b></td>
                                <td><?=$t_ok?></td>
                            </tr>
                            <tr>
                                <td><b>Pzas NO OK</b></td>
                                <td><?=$t_nok?></td>
                            </tr>
                            <?php foreach($d_fallas as $d_f):?>
                            <tr>
                                <td><b><?=$d_f->description?></b></td>
                                <td><?=$d_f->nok_pieces?></td>
                            </tr>
                            <?php endforeach;?>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>