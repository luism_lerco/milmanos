<div class="card card-default">
    <div class="card-header">
        <div class="card-title">Datos del Usuario</div>
    </div>
    <div class="card-block">
        <form class="form-horizontal ajaxPostForm" action="<?= $URL_ACTION ?>" method="POST" data-url-success="<?= $URL_SUCCESS ?>">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="part-number">No. Operador</label>
                        <input type="text" readonly class="form-control-plaintext" id="no_operador" value="<?=isset($USUARIO)?$USUARIO[0]->username:''?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="first_name">Nombre *</label>
                        <input type="text" name="first_name" class="form-control" placeholder="Nombre" value="<?= isset($USUARIO)?$USUARIO[0]->first_name:''?>"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="last_name">Paterno *</label>
                        <input type="text" name="last_name" class="form-control" placeholder="Paterno" value="<?= isset($USUARIO)?$USUARIO[0]->last_name:''?>"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="last_name_2">Materno *</label>
                        <input type="text" name="last_name_2" class="form-control" placeholder="Materno" value="<?= isset($USUARIO)?$USUARIO[0]->last_name_2:''?>"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="email">Correo Electrónico *</label>
                        <input type="email" name="email" class="form-control" autocomplete="off" placeholder="Correo Electrónico" autocomplete="off" value="<?= isset($USUARIO)?$USUARIO[0]->email:''?>"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="phone">Teléfono *</label>
                        <input type="tel" name="phone" class="form-control" autocomplete="off" placeholder="Teléfono" autocomplete="off" value="<?= isset($USUARIO)?$USUARIO[0]->phone:''?>"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="password">Contraseña *</label>
                        <input type="password" name="password" class="form-control" placeholder="Password" autocomplete="off" value="" />
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="confirmar_password">Confirmar Contraseña *</label>
                        <input type="password" name="confirmar_password" class="form-control" placeholder="Confirmar Password" autocomplete="off" value="" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-4">
                    <input type="hidden" class="form-control" name="id" id="id" value="<?= isset($USUARIO) ? $USUARIO[0]->id : '' ?>">
                    <button type="submit" class="btn btn-sm btn-primary m-r-5">Guardar</button>
                    <a href="<?= $URL_CANCELAR ?>" class="btn btn-sm btn-default">Cancelar</a>
                </div>
            </div>
        </form>
    </div>
</div>