<div class="modal-header clearfix text-left">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
</button>
    <h5><?= $HEADER_MODAL ?></h5>
</div>
<form role="form" method="post" action="<?= $URL_FORM ?>" class="ajaxPostFormModal" data-function-success="refresh_datatable">
    <div class="modal-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="first_name">Nombre *</label>
                    <input name="first_name" class="form-control" placeholder="Nombre" value="<?= (isset($CONTACTO) ? $CONTACTO[0]->first_name : '') ?>"/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="last_name">Paterno *</label>
                    <input name="last_name" class="form-control" placeholder="Paterno" value="<?= (isset($CONTACTO) ? $CONTACTO[0]->last_name : '') ?>"/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="last_name_2">Materno *</label>
                    <input name="last_name_2" class="form-control" placeholder="Materno" value="<?= (isset($CONTACTO) ? $CONTACTO[0]->last_name_2 : '') ?>"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label for="email">Correo Electrónico</label>
                    <input name="email" class="form-control" placeholder="Correo Electrónico" value="<?= (isset($CONTACTO) ? $CONTACTO[0]->email : '') ?>" autocomplete="off"/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="phone">Teléfono</label>
                    <input name="phone" class="form-control" placeholder="Teléfono" value="<?= (isset($CONTACTO) ? $CONTACTO[0]->phone : '') ?>"/>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-4">
                <div class="form-group">
                    <label for="username">Nombre de Usuario *</label>
                    <input name="username" class="form-control" placeholder="Usuario" value="<?= (isset($CONTACTO) ? $CONTACTO[0]->username : '') ?>" <?= (isset($CONTACTO) ? 'readonly="true" style="color:#000000"' : '') ?>/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="password">Password *</label>
                    <input type="password" name="password" class="form-control" placeholder="Password" value="" autocomplete="off"/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="password">Confirmar Password *</label>
                    <input type="password" name="password_confirm" class="form-control" placeholder="Confirmar Password" value="" autocomplete="off"/>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <input type="hidden" name="id_cliente" value="<?=$id_cliente?>" />
        <input type="hidden" name="id" value="<?= (isset($CONTACTO) ? $CONTACTO[0]->id : '') ?>"/>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</form>