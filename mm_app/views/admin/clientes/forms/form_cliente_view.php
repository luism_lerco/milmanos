<div class="modal-header clearfix text-left">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
</button>
    <h5><?= $HEADER_MODAL ?></h5>
</div>
<div id="file-uploader" class="hidden"></div>
<div id="file-uploader-movil" class="hidden"></div>
<form role="form" method="post" action="<?= $URL_FORM ?>" class="ajaxPostFormModal" data-function-success="refresh_datatable">
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="name">Nombre *</label>
                    <input name="name" class="form-control"
                           placeholder="Nombre" value="<?= (isset($CLIENTE) ? $CLIENTE[0]->name : '') ?>"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="street">Calle *</label>
                    <input name="street" class="form-control"
                           placeholder="Calle" value="<?= (isset($CLIENTE) ? $CLIENTE[0]->street : '') ?>"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="ext_num">No Ext. *</label>
                    <input name="ext_num" class="form-control"
                           placeholder="No. Exterior" value="<?= (isset($CLIENTE) ? $CLIENTE[0]->ext_num : '') ?>"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="int_num">No. Int</label>
                    <input name="int_num" class="form-control"
                           placeholder="No. Interior" value="<?= (isset($CLIENTE) ? $CLIENTE[0]->int_num : '') ?>"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label for="colony">Colonia *</label>
                    <input name="colony" class="form-control"
                           placeholder="Colonia" value="<?= (isset($CLIENTE) ? $CLIENTE[0]->colony : '') ?>"/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="cp">C.P.</label>
                    <input name="cp" class="form-control"
                           placeholder="Código Postal" value="<?= (isset($CLIENTE) ? $CLIENTE[0]->cp : '') ?>"/>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="municipality">Municipio</label>
                    <input name="municipality" class="form-control"
                           placeholder="Municipio" value="<?= (isset($CLIENTE) ? $CLIENTE[0]->municipality : '') ?>"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="state">Estado</label>
                    <select name="state" class="form-control">
                        <option value="Aguascalientes" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Aguascalientes')?'selected="selected"':''?>>Aguascalientes</option>
                        <option value="Baja California" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Baja California')?'selected="selected"':''?>>Baja California</option>
                        <option value="Baja California Sur" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Baja California Sur')?'selected="selected"':''?>>Baja California Sur</option>
                        <option value="Campeche" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Campeche')?'selected="selected"':''?>>Campeche</option>
                        <option value="Coahuila" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Coahuila')?'selected="selected"':''?>>Coahuila de Zaragoza</option>
                        <option value="Colima" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Colima')?'selected="selected"':''?>>Colima</option>
                        <option value="Chiapas" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Chiapas')?'selected="selected"':''?>>Chiapas</option>
                        <option value="Chihuahua" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Chihuahua')?'selected="selected"':''?>>Chihuahua</option>
                        <option value="Distrito Federal" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Distrito Federal')?'selected="selected"':''?>>Distrito Federal</option>
                        <option value="Durango" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Durango')?'selected="selected"':''?>>Durango</option>
                        <option value="Guanajuato" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Guanajuato')?'selected="selected"':''?>>Guanajuato</option>
                        <option value="Guerrero" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Guerrero')?'selected="selected"':''?>>Guerrero</option>
                        <option value="Hidalgo" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Hidalgo')?'selected="selected"':''?>>Hidalgo</option>
                        <option value="Jalisco" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Jalisco')?'selected="selected"':''?>>Jalisco</option>
                        <option value="México" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'México')?'selected="selected"':''?>>México</option>
                        <option value="Michoacán" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Michoacán')?'selected="selected"':''?>>Michoacán</option>
                        <option value="Morelos" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Morelos')?'selected="selected"':''?>>Morelos</option>
                        <option value="Nayarit" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Nayarit')?'selected="selected"':''?>>Nayarit</option>
                        <option value="Nuevo León" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Nuevo León')?'selected="selected"':''?>>Nuevo León</option>
                        <option value="Oaxaca" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Oaxaca')?'selected="selected"':''?>>Oaxaca</option>
                        <option value="Puebla" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Puebla')?'selected="selected"':''?>>Puebla</option>
                        <option value="Querétaro" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Querétaro')?'selected="selected"':''?>>Querétaro</option>
                        <option value="Quintana Roo" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Quintana Roo')?'selected="selected"':''?>>Quintana Roo</option>
                        <option value="San Luis Potosí" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'San Luis Potosí')?'selected="selected"':''?>>San Luis Potosí</option>
                        <option value="Sinaloa" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Sinaloa')?'selected="selected"':''?>>Sinaloa</option>
                        <option value="Sonora" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Sonora')?'selected="selected"':''?>>Sonora</option>
                        <option value="Tabasco" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Tabasco')?'selected="selected"':''?>>Tabasco</option>
                        <option value="Tamaulipas" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Tamaulipas')?'selected="selected"':''?>>Tamaulipas</option>
                        <option value="Tlaxcala" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Tlaxcala')?'selected="selected"':''?>>Tlaxcala</option>
                        <option value="Veracruz" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Veracruz')?'selected="selected"':''?>>Veracruz</option>
                        <option value="Yucatán" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Yucatán')?'selected="selected"':''?>>Yucatán</option>
                        <option value="Zacatecas" <?=(isset($CLIENTE) && $CLIENTE[0]->state == 'Zacatecas')?'selected="selected"':''?>>Zacatecas</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <input type="hidden" name="id" value="<?= (isset($CLIENTE) ? $CLIENTE[0]->id_client : '') ?>"/>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</form>