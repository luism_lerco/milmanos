            </div>
          <!-- END CONTAINER FLUID -->
        </div>
        <!-- END PAGE CONTENT -->
        <!-- START FOOTER -->
        <div class="container-fluid container-fixed-lg footer">
          <div class="copyright sm-text-center">
            <p class="small no-margin pull-left sm-pull-reset">
              <span class="hint-text">Copyright © <?=date('Y')?></span>
              <span class="font-montserrat">Quality Improvement</span>.
              <span class="hint-text">Todos los derechos Reservados.</span>
              <span class="sm-block"><a href="#" class="m-l-10">Políticas de Privacidad</a></span>
            </p>
            <p class="small no-margin pull-right sm-pull-reset">
                <span class="hint-text">Desarrollado por ®</span>
                <a target="_blank" href="http://www.lerco.mx">Lerco</a>
            </p>
            <div class="clearfix"></div>
          </div>
        </div>
        <!-- END FOOTER -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTAINER -->
    <!-- BEGIN VENDOR JS -->
    <script type="text/javascript" src="<?=base_url('assets/plugins/feather-icons/feather.js')?>"></script>
    <!-- BEGIN VENDOR JS -->
    <script type="text/javascript" src="<?=base_url('assets/plugins/pace/pace.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery/jquery-1.11.1.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/modernizr.custom.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery-ui/jquery-ui.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/tether/js/tether.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/bootstrap/js/bootstrap.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery/jquery-easy.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery-unveil/jquery.unveil.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery-bez/jquery.bez.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery-ios-list/jquery.ioslist.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/imagesloaded/imagesloaded.pkgd.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery-actual/jquery.actual.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js')?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/plugins/file-upload/fileuploader.min.js')?>"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script type="text/javascript" src="<?=base_url('assets/pages/js/pages.js')?>"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script type="text/javascript" src="<?=base_url('assets/js/custom_dialogs.js') ?>"></script>
    <script type="text/javascript" src="<?=base_url('assets/js/scripts_alt.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/js/functions.js') ?>"></script>
    <script type="text/javascript">
      $(document).ready(function () {
        RootJS = '<?= base_url() ?>';
<?php
if (isset($link_active) && is_array($link_active)) {
    foreach ($link_active as $lnk) {
        ?>
        $("<?= $lnk ?>").addClass('active');
        <?php
    }
}
?>
      });
    </script>
    <?=isset($js)?$js:''?>
  </body>
</html>