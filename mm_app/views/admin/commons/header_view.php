<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title><?=$title?> - Mil Manos</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/pace/pace-theme-flash.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/bootstrap/css/bootstrap.min.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/font-awesome/css/font-awesome.css')?>" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/jquery-scrollbar/jquery.scrollbar.css')?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/plugins/select2/css/select2.min.css')?>" media="screen" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/pages/css/pages-icons.css')?>">
    <?=isset($css)?$css:''?>
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/pages/css/themes/light.css')?>" class="main-stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/style.css')?>" class="main-stylesheet" />
  </head>
  <body class="fixed-header menu-pin">
    <?=$this->load->view('admin/commons/custom_dialogs.php', '', true) ?>
    <!-- BEGIN SIDEBAR -->
        <!-- BEGIN SIDEBPANEL-->
    <nav class="page-sidebar" data-pages="sidebar">
      <!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->
      <div class="sidebar-overlay-slide from-top" id="appMenu">

      </div>
      <!-- END SIDEBAR MENU TOP TRAY CONTENT-->
      <!-- BEGIN SIDEBAR MENU HEADER-->
      <div class="sidebar-header">
        <img src="<?=base_url('assets/img/logo_dashboard.png')?>" alt="logo" class="brand" data-src="<?=base_url('assets/img/logo_dashboard.png')?>" data-src-retina="<?=base_url('assets/img/logo_dashboard.png')?>" width="198" height=60>
      </div>
      <!-- END SIDEBAR MENU HEADER-->
      <!-- START SIDEBAR MENU -->
      <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
            <li id='liInicio' class="m-t-30">
                <a href="<?=base_url('admin/inicio')?>">
                    <span class="title">Inicio</span>
                </a>
                <span class="icon-thumbnail "><i data-feather="home"></i></span>
            </li>
            <?php if($this->ion_auth->is_admin()):?>
            <li id="liOperadores">
                <a href="<?=base_url('admin/operadores')?>">
                    <span class="title">Operadores</span>
                </a>
                <span class="icon-thumbnail "><i data-feather="user"></i></span>
            </li>
            <li id="liHtcs">
                <a href="<?=base_url('admin/htcs')?>">
                    <span class="title">HI</span>
                </a>
                <span class="icon-thumbnail "><i data-feather="list"></i></span>
            </li>
            <li id="liProveedores">
                <a href="<?=base_url('admin/proveedores')?>">
                    <span class="title">Proveedores</span>
                </a>
                <span class="icon-thumbnail "><i data-feather="truck"></i></span>
            </li>
            <?php endif;?>
            <?php if($this->ion_auth->in_group([1,3])):?>
            <li id="liReportes">
                <a href="javascript:;">
                    <span class="title">Reportes</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i data-feather="package"></i></span>
                <ul class="sub-menu">
                    <li id="lkProduccion">
                        <a href="<?=base_url('admin/reportes')?>">Producción</a>
                        <span class="icon-thumbnail">P</span>
                    </li>
                    <?php if($this->ion_auth->is_admin()):?>
                    <li id="lkOperadores" class="">
                        <a href="<?=base_url('admin/reportes/operadores')?>">Operadores</a>
                        <span class="icon-thumbnail">O</span>
                    </li>
                    <?php endif;?>
                </ul>
            </li>
            <?php endif;?>
            <?php if($this->ion_auth->is_admin()):?>
            <li id="liConfiguracion">
                <a href="javascript:;">
                    <span class="title">Configuración</span>
                    <span class=" arrow"></span>
                </a>
                <span class="icon-thumbnail"><i data-feather="settings"></i></span>
                <ul class="sub-menu">
                    <li id="lkClientes">
                        <a href="<?=base_url('admin/clientes')?>">Clientes</a>
                        <span class="icon-thumbnail">C</span>
                    </li>
                    <li id="lkAreas" class="">
                        <a href="<?=base_url('admin/areas')?>">Áreas</a>
                        <span class="icon-thumbnail">A</span>
                    </li>
                    <li id="lkTurnos" class="">
                        <a href="<?=base_url('admin/turnos')?>">Turnos</a>
                        <span class="icon-thumbnail">T</span>
                    </li>
                </ul>
            </li>
            <?php endif;?>
            <li class="hidden-md-up">
                <a href="<?=base_url('admin/salir')?>">
                    <span class="title">Salir</span>
                </a>
                <span class="icon-thumbnail "><i data-feather="power"></i></span>
            </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->
    </nav>
    <!-- END SIDEBAR -->
    <!-- END SIDEBAR -->
    <!-- START PAGE-CONTAINER -->
    <div class="page-container">
      <!-- START PAGE HEADER WRAPPER -->
      <!-- START HEADER -->
      <div class="header ">
        <!-- START MOBILE SIDEBAR TOGGLE -->
        <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu" data-toggle="sidebar">
        </a>
        <!-- END MOBILE SIDEBAR TOGGLE -->
        <div class="">
          <div class="brand inline">
            <img src="<?=base_url('assets/img/logo.png')?>" alt="logo" data-src="<?=base_url('assets/img/logo.png')?>" data-src-retina="<?=base_url('assets/img/logo.png')?>" width="37" height="22">
          </div>
        </div>
        <div class="d-flex align-items-center">
          <!-- START User Info-->
          <div class="pull-left p-r-10 fs-14 font-heading hidden-md-down m-l-20">
            <span class="semi-bold"><?=$this->session->userdata('name')?></span>
          </div>
          <div class="dropdown pull-right hidden-md-down">
            <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="thumbnail-wrapper d32 circular inline">
              <img src="<?=base_url('assets/img/perfiles/avatar.jpg')?>" alt="" data-src="<?=base_url('assets/img/perfiles/avatar.jpg')?>" data-src-retina="<?=base_url('assets/img/perfiles/avatar_small2x.jpg')?>" width="32" height="32">
              </span>
            </button>
            <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
              <!--<a href="<?=base_url('admin/mi-perfil')?>" class="dropdown-item"><i class="pg-outdent"></i> Mi Perfil</a>-->
              <a href="<?=base_url('admin/salir')?>" class="clearfix bg-master-lighter dropdown-item">
                <span class="pull-left">Salir</span>
                <span class="pull-right"><i class="pg-power"></i></span>
              </a>
            </div>
          </div>
          <!-- END User Info-->
        </div>
      </div>
      <!-- END HEADER -->
      <!-- END PAGE HEADER WRAPPER -->
      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper">
        <!-- START PAGE CONTENT -->
        <div class="content">
          <div class=" container-fluid container-fixed-lg sm-p-l-0 sm-p-r-0">
            <div class="inner no-print">
              <!-- START BREADCRUMB -->
              <?php if (isset($breadcumb)) { ?>
                  <!-- begin breadcrumb -->
                  <ol class="breadcrumb">
                      <?php
                      for ($i = 0; $i < sizeof($breadcumb); $i++) {
                          if ($i == (sizeof($breadcumb) - 1)) {
                              echo '<li class="breadcrumb-item active">' . $breadcumb[$i]['label'];
                          } else {
                              echo '<li class="breadcrumb-item">';
                              echo '<a href="' . (isset($breadcumb[$i]['url']) ? $breadcumb[$i]['url'] : 'javascript:;') . '">';
                              echo $breadcumb[$i]['label'] . '</a>';
                          }

                          echo '</li>';
                      }
                      ?>
                  </ol>
                  <!-- end breadcrumb -->
              <?php } ?>
              <!-- END BREADCRUMB -->
            </div>
          </div>
          <!-- START CONTAINER FLUID -->
          <div class="container-fluid container-fixed-lg">