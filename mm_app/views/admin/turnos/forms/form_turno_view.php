<div class="modal-header clearfix text-left">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
</button>
    <h5><?= $HEADER_MODAL ?></h5>
</div>
<div id="file-uploader" class="hidden"></div>
<div id="file-uploader-movil" class="hidden"></div>
<form role="form" method="post" action="<?= $URL_FORM ?>" class="ajaxPostFormModal" data-function-success="refresh_datatable">
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="name">Nombre *</label>
                    <input name="name" class="form-control"
                           placeholder="Nombre" value="<?= (isset($TURNO) ? $TURNO[0]->name : '') ?>"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="description">Descripción *</label>
                    <textarea name="description" class="form-control" rows="3" placeholder="Descripción"><?= (isset($TURNO) ? $TURNO[0]->description : '') ?></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="time_start">Hora Inicio</label><br>
                    <div class="input-group bootstrap-timepicker timepicker">
                        <input id="time_start" name="time_start" type="text" class="form-control" data-minute-step="15" data-show-inputs="false" data-template="dropdown" data-show-seconds="false" data-show-meridian="false" data-default-time="<?=(isset($TURNO) ? $TURNO[0]->time_start : 'current');?>">
                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="time_end">Hora Fin</label><br>
                    <div class="input-group bootstrap-timepicker timepicker">
                        <input id="time_end" name="time_end" type="text" class="form-control" data-minute-step="15" data-show-inputs="false" data-template="dropdown" data-show-seconds="false" data-show-meridian="false" data-default-time="<?=(isset($TURNO) ? $TURNO[0]->time_end : 'current');?>">
                        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <input type="hidden" name="id" value="<?= (isset($TURNO) ? $TURNO[0]->id_turn : '') ?>"/>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
</form>