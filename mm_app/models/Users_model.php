<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_model extends MY_model {

    protected $table = 'users';
    protected $primary_key = 'users.id';

    public function __construct() {
        parent::__construct();
    }

    public function get($id = NULL, $order_by = NULL, $like = null, $desactivarComillas = null) {
        if (is_numeric($id)) {
            $this->db->where($this->primary_key, $id);
        }
        if (is_array($id)) {
            foreach ($id as $_key => $_value) {
                $this->db->where($_key, $_value, $desactivarComillas);
            }
        }
        if(is_string($order_by)){
            $this->db->order_by($order_by);
        }
        if (is_array($order_by)) {
            foreach ($order_by as $_value) {
                $this->db->order_by($_value);
            }
        }

        if($like != null){
            if(is_array($like)){
                foreach ($like as $k => $v){
                    $this->db->like($k, $v);
                }
            }
        }


        $this->db->select('users.id as id, username, email, first_name, last_name, last_name_2, last_login, company, phone, password, users.active, users.id_plant,  IFNULL(clients.name, "") AS plant, users.id_area, IFNULL(areas.name, "")  AS area, users.id_turn, IFNULL(turns.name, "") AS turn', false);
        $this->db->from($this->table);
        $this->db->join('users_groups', 'users_groups.user_id = users.id');
        $this->db->join('clients', 'clients.id_client = users.id_plant', 'left');
        $this->db->join('areas', 'areas.id_area = users.id_area', 'left');
        $this->db->join('turns', 'turns.id_turn = users.id_turn', 'left');
        $this->db->order_by('users.id', 'desc');

        $q = $this->db->get();
        return $q->result();
    }

    function getUserbyUsername($username,$GID){
        $this->db->select('users.id as uid, username, email, first_name, last_name, company, phone, password, users.id_plant,  IFNULL(clients.name, "") AS plant, users.id_area, IFNULL(areas.name, "")  AS area, users.id_turn, IFNULL(turns.name, "") AS turn', false);
        $this->db->from('users');
        $this->db->join('users_groups', 'users_groups.user_id = users.id');
        $this->db->join('clients', 'clients.id_client = users.id_plant', 'left');
        $this->db->join('areas', 'areas.id_area = users.id_area', 'left');
        $this->db->join('turns', 'turns.id_turn = users.id_turn', 'left');
        $this->db->where('users.username', $username);
        $this->db->where('users.active', '1');
        $this->db->where('users_groups.group_id', $GID);
        $this->db->order_by('users.id', 'desc');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }

    function getUserbyID($ID, $GID){
        $this->db->select('users.id as uid, username, email, first_name, last_name, company, phone, password, users.id_plant,  IFNULL(clients.name, "") AS plant, users.id_area, IFNULL(areas.name, "")  AS area, users.id_turn, IFNULL(turns.name, "") AS turn', false);
        $this->db->from('users');
        $this->db->join('users_groups', 'users_groups.user_id = users.id');
        $this->db->join('clients', 'clients.id_client = users.id_plant', 'left');
        $this->db->join('areas', 'areas.id_area = users.id_area', 'left');
        $this->db->join('turns', 'turns.id_turn = users.id_turn', 'left');
        $this->db->where('users.id', $ID);
        $this->db->where('users.active', '1');
        $this->db->where('users_groups.group_id', $GID);
        $this->db->order_by('users.id', 'desc');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }

    function setWSToken($UID){
        $wst = $this->getWSToken($UID);
        if( !empty($wst) ){
            return '';
        }
        $time = time();
        $token = sha1($time.$UID.rand(1,999999));
        $data = array('UID_req'=>$UID, 'time'=>$time,'key'=>$token);
        $this->db->insert('ws_access', $data);
        return $token;
    }

    function getWSToken($UID){
        $this->db->select('*');
        $this->db->from('ws_access');
        $this->db->where('UID_req', $UID);
        $this->db->order_by('UID_req', 'desc');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row();
    }

    function updateWSToken($UID){
        $time = time();
        $token = sha1($time.$UID.rand(1,999999));
        $data = array('time'=>$time,'key'=>$token);
        $this->db->where('UID_req', $UID);
        $this->db->update('ws_access', $data);
    }
}