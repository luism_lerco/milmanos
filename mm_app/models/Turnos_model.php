<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Turnos_model extends MY_model {

    protected $table = 'turns';
    protected $primary_key = 'id_turn';

    public function __construct() {
        parent::__construct();
    }
}

?>