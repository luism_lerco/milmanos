<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_register_turns_model extends MY_model {

    protected $table = 'users_register_turns';
    protected $primary_key = 'id_user_register_turn';

    public function __construct() {
        parent::__construct();
    }
}

?>