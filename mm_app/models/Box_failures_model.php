<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Box_failures_model extends MY_model {

    protected $table = 'box_failures';
    protected $primary_key = 'id_box_failure';

    public function __construct() {
        parent::__construct();
    }

    public function get_items($id = NULL, $order_by = NULL, $like = NULL, $desactivarComillas = NULL) {
        if (is_numeric($id)) {
            $this->db->where($this->primary_key, $id);
        }
        if (is_array($id)) {
            foreach ($id as $_key => $_value) {
                $this->db->where($_key, $_value, $desactivarComillas);
            }
        }
        if(is_string($order_by)){
            $this->db->order_by($order_by);
        }
        if (is_array($order_by)) {
            foreach ($order_by as $_value) {
                $this->db->order_by($_value);
            }
        }

        if($like != null){
            if(is_array($like)){
                foreach ($like as $k => $v){
                    $this->db->like($k, $v);
                }
            }
        }

        $this->db->select('SUM('.$this->table.'.nok_pieces) as nok_pieces, failures.image, failures.description, failures.id_htc');
        $this->db->from($this->table);
        $this->db->join('failures', $this->table.'.id_failure = failures.id_failure');
        $this->db->group_by($this->table.'.id_failure');
        $q = $this->db->get();
        return $q->result();
    }


    public function get_items_in($id = NULL, $order_by = NULL, $like = NULL, $desactivarComillas = NULL) {
        if (is_numeric($id)) {
            $this->db->where_in($this->primary_key, $id);
        }
        if (is_array($id)) {
            foreach ($id as $_key => $_value) {
                $this->db->where_in($_key, $_value, $desactivarComillas);
            }
        }
        if(is_string($order_by)){
            $this->db->order_by($order_by);
        }
        if (is_array($order_by)) {
            foreach ($order_by as $_value) {
                $this->db->order_by($_value);
            }
        }

        if($like != null){
            if(is_array($like)){
                foreach ($like as $k => $v){
                    $this->db->like($k, $v);
                }
            }
        }

        $this->db->select('SUM('.$this->table.'.nok_pieces) as nok_pieces, failures.image, failures.description, failures.id_htc');
        $this->db->from($this->table);
        $this->db->join('failures', $this->table.'.id_failure = failures.id_failure');
        $this->db->group_by($this->table.'.id_failure');
        $q = $this->db->get();
        return $q->result();
    }
}
?>