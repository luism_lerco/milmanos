<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Htcs_model extends MY_model {

    protected $table = 'htcs';
    protected $primary_key = 'id_htc';

    public function __construct() {
        parent::__construct();
    }

    public function get($id = NULL, $order_by = NULL, $like = null, $desactivarComillas = null) {
        if (is_numeric($id)) {
            $this->db->where($this->primary_key, $id);
        }
        if (is_array($id)) {
            foreach ($id as $_key => $_value) {
                $this->db->where($_key, $_value, $desactivarComillas);
            }
        }
        if(is_string($order_by)){
            $this->db->order_by($order_by);
        }
        if (is_array($order_by)) {
            foreach ($order_by as $_value) {
                $this->db->order_by($_value);
            }
        }

        if($like != null){
            if(is_array($like)){
                foreach ($like as $k => $v){
                    $this->db->like($k, $v);
                }
            }
        }

        $this->db->select($this->table.'.*, providers.name as  proveedor, clients.name as cliente');
        $this->db->from($this->table);
        $this->db->join('providers', 'providers.id_provider='.$this->table.'.id_provider');
        $this->db->join('clients', 'clients.id_client='.$this->table.'.id_client');
        $q = $this->db->get();
        return $q->result();
    }
}

?>