<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Clientes_model extends MY_model {

    protected $table = 'clients';
    protected $primary_key = 'id_client';

    public function __construct() {
        parent::__construct();
    }
}
?>