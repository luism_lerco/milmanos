<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Proveedores_model extends MY_model {

    protected $table = 'providers';
    protected $primary_key = 'id_provider';

    public function __construct() {
        parent::__construct();
    }
}

?>