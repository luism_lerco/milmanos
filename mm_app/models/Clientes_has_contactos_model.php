<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Clientes_has_contactos_model extends MY_model {

    protected $table = 'clients_has_contacts';

    public function __construct() {
        parent::__construct();
    }
}

?>