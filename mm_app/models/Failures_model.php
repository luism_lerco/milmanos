<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Failures_model extends MY_model {

    protected $table = 'failures';
    protected $primary_key = 'id_failure';

    public function __construct() {
        parent::__construct();
    }

    public function get($id = NULL, $order_by = NULL, $like = null, $desactivarComillas = null) {
        if (is_numeric($id)) {
            $this->db->where($this->primary_key, $id);
        }
        if (is_array($id)) {
            foreach ($id as $_key => $_value) {
                $this->db->where($_key, $_value, $desactivarComillas);
            }
        }
        if(is_string($order_by)){
            $this->db->order_by($order_by);
        }
        if (is_array($order_by)) {
            foreach ($order_by as $_value) {
                $this->db->order_by($_value);
            }
        }

        if($like != null){
            if(is_array($like)){
                foreach ($like as $k => $v){
                    $this->db->like($k, $v);
                }
            }
        }

        $this->db->select($this->table.'.*, htcs.part_number');
        $this->db->from($this->table);
        $this->db->join('htcs', 'htcs.id_htc='.$this->table.'.id_htc');
        $q = $this->db->get();
        return $q->result();
    }
}

?>