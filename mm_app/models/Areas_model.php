<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Areas_model extends MY_model {

    protected $table = 'areas';
    protected $primary_key = 'id_area';

    public function __construct() {
        parent::__construct();
    }
}

?>