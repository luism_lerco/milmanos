<?php
	require('valums.php');
	$allowedExtensions = array();
	$sizeLimit = 2 * 1024 * 1024;
	$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
	$result = $uploader->handleUpload('uploads/');
	$result['name']=$uploader->getUploadName();
	//$result['metodo']=$uploader->metodo;
	$result['oldName']=$uploader->getName();
	echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
?>