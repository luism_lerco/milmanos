function showLoader(Titulo) {
    $('#divLoader h5').html(Titulo);
    $('#divLoader').modal('show');
}
function closeLoader() {
    $('#divLoader').modal('hide');
    $('.modal-backdrop').remove();
}
function showSubLoader(Titulo) {
    $('#divSubLoader h5').html(Titulo);
    $('#divSubLoader').modal('show');
}
function closeSubLoader() {
    setTimeout(function(){ $('#divSubLoader').modal('toggle'); }, 3000);
    //$('.modal-backdrop').remove();
    //$("#body").removeClass("modal-open");
}
function showAlert(Titulo, Texto) {
	$('#divAlert h4').html(Titulo);
	$("#divAlert .modal-body").html(Texto);
	if(Titulo == 'Éxito'){
        $('#divAlert .modal-header').removeClass('bg-danger');
        $('#divAlert .modal-header').addClass('bg-success');
	}
	else{
        $('#divAlert .modal-header').removeClass('bg-success');
        $('#divAlert .modal-header').addClass('bg-danger');
	}
	$('#divAlert').modal('show');
}

function showSuccess(Titulo, Texto) {
    $('#divSuccess h4').html(Titulo);
    $("#divSuccess .modal-body").html(Texto);
    $('#divSuccess').modal('show');
}
function showConfirm(Titulo, Texto, Funcion, funcionCancelar) {
    $('#divConfirm h5').html(Titulo);
    $("#divConfirm .modal-body").html(Texto);
    $("#divConfirm .btn-default").on("click", function(){
    	if (typeof (funcionCancelar) === 'function') {
    		console.log('Cancelo');
    		funcionCancelar();
    	}
        $('#divConfirm').modal('hide');
    });
    $("#divConfirm .btn-primary").off("click");
    $("#divConfirm .btn-primary").on('click', function() {
        Funcion();
        $('#divConfirm').modal('hide');
    });
    $('#divConfirm').modal('show');
}

function loadModal(url, data) {
    showLoader('Espera un momento...');
    $('#myModal .modal-content').html('');
    $.post(url, data, function(response) {
        closeLoader();
        $('#myModal .modal-content').append(response);
        setTimeout(function(){
            $('#myModal').modal('show');
        },500);
    });
    return  false;
}

function loadBigModal(url, data) {
    showLoader('Espera un momento...');
    $('#myBigModal .modal-content').html('');
    $.post(url, data, function(response) {
        closeLoader();
        $('#myBigModal .modal-content').append(response);
        setTimeout(function(){
            $('#myBigModal').modal('show');
        },400);
    });
    return  false;
}

function showModal(Texto) {
    $("#myModal .modal-content").html(Texto);
    $('#myModal').modal('show');
}

function showModalBig(Texto) {
    $("#myBigModal .modal-content").html(Texto);
    $('#myBigModal').modal('show');
}

function showConfirmB(Titulo, Texto, Funcion, FunctionCancelar, objeto) {
    $('#divConfirm h4').html(Titulo);
    $("#divConfirm .modal-body").html(Texto);
    $("#divConfirm .btn-default").on('click', function() {
        FunctionCancelar(objeto);
        $('#divConfirm').modal('hide');
    });
    $("#divConfirm .btn-primary").on('click', function() {
        Funcion();
        $('#divConfirm').modal('hide');
    });
    $('#divConfirm').modal('show');
}

$('#myModal').bind('hidden.bs.modal', function () {
  $("html").css("margin-right", "0px");
});
$('#myModal').bind('show.bs.modal', function () {
  $("html").css("margin-right", "-15px");
});
$('#myBigModal').bind('hidden.bs.modal', function () {
  $("html").css("margin-right", "0px");
});
$('#myBigModal').bind('show.bs.modal', function () {
  $("html").css("margin-right", "-15px");
});